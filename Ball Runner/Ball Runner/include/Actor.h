//----------------------------------------------------------------------------------------------------------------------
/// \file Actor.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The base class for all 3D Game Actors for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __ACTOR__
#define __ACTOR__

#pragma once
#include <memory>
#include "gl\glew.h"
#include "glm\gtc\type_ptr.hpp"

//----------------------------------------------------------------------------------------------------------------------
/// \name Fowards Declarations
/// \brief All classes forward declared here
//----------------------------------------------------------------------------------------------------------------------
/// \brief Model Declaration
namespace asset { class model; }
/// \brief ModelManager Declaration
namespace asset { class ModelManager; }
/// \brief Dynamics World Declaration
class btDiscreteDynamicsWorld;
/// \brief Bullet Rigid Body Declaration
class btRigidBody;
/// \brief Bullet Vector 3 Declaration
class btVector3;
//----------------------------------------------------------------------------------------------------------------------
/// \name Base Actor Class
/// \brief The base class for all 3D Actors
//----------------------------------------------------------------------------------------------------------------------
class Actor
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	Actor();
	/// \brief Class Destructor
	~Actor();
	/// \brief Initialize ModelManager within the Class
	/// \param Pointer const to the Model Manager
	/// \param Const char pointer const to the mesh file path
	/// \param Const char pointer const to the texture file path
	/// \param Const GLenum to the TextureTarget
	/// \return Returns boolean true if successful
	bool Setup(std::shared_ptr<asset::ModelManager> &mManager, const char* const meshpath, const char* const texturepath, const GLenum textureTarget);
	/// \brief Update call to update the Model Matrix
	void UpdateActor();
	/// \brief Uses the Model Manager to Draw the Actor
	void Draw3D();
protected:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Protected Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Vec3 to Scale Actor
	glm::vec3 m_scale;
	/// \brief Vec3 to Rotate Actor
	glm::vec3 m_rotate;
	/// \brief Vec3 to Translate Actor
	glm::vec3 m_pos;
	/// \brief Mat4x4 to the Model Matrix
	glm::mat4 m_modelMatrix;
private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Unique Pointer to Model Data
	std::unique_ptr<asset::model> m_model;
	/// \brief Shared Pointer to the Model Manager
	std::shared_ptr<asset::ModelManager> m_modelManager;
};

#endif // __ACTOR__