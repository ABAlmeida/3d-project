//----------------------------------------------------------------------------------------------------------------------
/// \file Application.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The main application class for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __APPLICATION__
#define __APPLICATION__

#pragma once
#include <memory>
#include <SDL.h>

//----------------------------------------------------------------------------------------------------------------------
/// \name Fowards Declarations
/// \brief All classes forward declared here
//----------------------------------------------------------------------------------------------------------------------
/// \brief Menu Declaration
class Menu;
/// \brief Level1 Declaration
class Level1;
/// \brief MiniApp Declaration
class MiniApp;
/// \brief Bullet Physics Declaration
class bPhysics;
/// \brief Graphics Declaration
class Graphics;
/// \brief Input Declaration
namespace input { class inputReader; }
/// \brief ModelManager Declaration
namespace asset { class ModelManager; }

//----------------------------------------------------------------------------------------------------------------------
/// \name Application Class
//----------------------------------------------------------------------------------------------------------------------
class App
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	App();
	~App();
	/// \brief Class Initialisor
	/// \param Const Int Width
	/// \param Const Int Height
	void onInitialise(const int w, const int h);
	/// \brief Class Input Handling
	/// \return Boolean (True to continue running App)
	bool onInput();
	/// \brief Class Update Function
	void onUpdate();
	/// \brief Class Draw Function
	void onDraw();
	/// \brief Class UI Draw Function
	void onGUI();

	/// \brief Calculates dt between frames
	/// \return Delta Time as a Float
	inline float deltaTime()
	{
		unsigned int current = SDL_GetTicks();
		// Next, we want to work out the change in time between the previous frame and the current one
		// This is a 'delta' (used in physics to denote a change in something)
		// So we call it the 'deltaT' and I like to use an 's' to remind me that it's in seconds!
		// (To get it in seconds we need to divide by 1000 to convert from milliseconds)
		float deltaTs = static_cast<float>(current - m_last) * 0.001f;
		m_last = current;
		return deltaTs;
	}

private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Unique Pointer to the Active Mini Application within out Application
	std::shared_ptr<MiniApp> m_ActiveApp;

	/// \brief Unique Pointer to the Input Handling Class
	std::unique_ptr<input::inputReader> m_input;
	/// \brief Unique Pointer to the Graphics Class
	std::unique_ptr<Graphics> m_graphics;
	/// \brief Shared Pointer to the Model Managing Class
	std::shared_ptr<asset::ModelManager> m_modelManager;
	/// \brief Shared Pointer to the Bullet Physics Class
	bPhysics* m_physics;

	/// \brief Unique Pointer to the Menu Mini Application Class
	std::shared_ptr<Menu> m_menu;
	/// \brief Unique Pointer to the Menu Mini Application Class
	std::shared_ptr<Level1> m_level1;

	/// \brief SDL Event
	SDL_Event m_event;
	/// \brief Input Event handled within Enum
	int m_inputEvent;

	/// \brief Current dt value
	float m_dt;
	/// \brief Last dt Value
	unsigned int m_last;
	/// \brief Tracks the currently active Mini Application
	int m_mode;
};

#endif // __APPLICATION__