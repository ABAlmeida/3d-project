//----------------------------------------------------------------------------------------------------------------------
/// \file Ball.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The Class for the game object , Ball, inheriting for Actor.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __BALL__
#define __BALL__

#pragma once
#include "Actor.h"

//----------------------------------------------------------------------------------------------------------------------
/// \name Ball Class
/// \brief Inherits from Actor
//----------------------------------------------------------------------------------------------------------------------
class Ball : public Actor
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	Ball();
	/// \brief Class Destructor
	~Ball();
	/// \brief Initialise the class
	/// \param Pointer const to the Model Manager
	/// \param Pointer const to the bullet discrete dynamics world
	void Initialise(std::shared_ptr<asset::ModelManager> &mManager, btDiscreteDynamicsWorld* const dynamicsWorld);
	/// \brief Update call for the Actor
	void Update(float dt);
	/// \brief Draw call for the Actor
	void Draw();
	/// \brief Modify the ball position
	/// \param Const vec3 reference to modify the position
	void modifyPos(const glm::vec3 &pos);
	/// \brief Get the ball transform from the Physics engine
	void getTrans();
	/// \brief Reset the Ball position
	void resetBall();
	/// \brief Get the ball Position
	/// \return Returns the vec3 position of the ball
	inline glm::vec3 getPos()
	{ return m_pos; }
	/// \brief Checks AABB between the Vectors
	/// \param Vec3 reference to the min
	/// \param Vec3 reference to the max
	void getAABB(btVector3 &min, btVector3 &max);
private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Physics Data
	std::unique_ptr<btRigidBody> m_RigidBody;
};

#endif // __BALL__