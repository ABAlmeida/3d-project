//----------------------------------------------------------------------------------------------------------------------
/// \file BaseLevel.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The base Level class for the game.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __BASELEVEL__
#define __BASELEVEL__

#pragma once
#include <memory>
#include "MiniApp.h"

//----------------------------------------------------------------------------------------------------------------------
/// \name Fowards Declarations
/// \brief All classes forward declared here
//----------------------------------------------------------------------------------------------------------------------
/// \brief Model Manager Declaration
namespace asset { class ModelManager; }
/// \brief Bullet Dynamics World Declaration
class btDiscreteDynamicsWorld;
/// \brief Bullet Vec3 Declaration
class btVector3;
/// \brief Ball Declaration
class Ball;
/// \brief Goal Declaration
class Goal;
//----------------------------------------------------------------------------------------------------------------------
/// \name Base Level Class
/// \brief Publicly inherits from Mini App
/// \brief Inherit from this to make levels
//----------------------------------------------------------------------------------------------------------------------
class BaseLevel : public MiniApp
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	BaseLevel();
	/// \brief Virtual Class Destructor
	virtual ~BaseLevel();
	/// \brief Virtual Intialise Override
	/// \param Standard Shared Pointer reference to Model Manager
	/// \param Pointer const to the Dynamics World
	virtual void onInitialise(std::shared_ptr<asset::ModelManager> &mManager, btDiscreteDynamicsWorld* const dynamicsWorld);
	/// \brief Virtual Input Override
	/// \param Const int input
	/// \return Returns MiniApp Change
	virtual int onInput(const int input);
	/// \brief Virtual Update Override
	/// \param Const float Delta Time
	virtual void onUpdate(const float dt);
	/// \brief Virtual Draw Override
	virtual void onDraw();
	/// \brief Virtual UI Draw Override
	virtual void onGUI();
protected:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Protected Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Checks the AABB collision between two objects
	/// \param Const vec3 to first object minimum position
	/// \param Const vec3 to first object maximum position
	/// \param Const vec3 to second object minimum position
	/// \param Const vec3 to second object maximum position
	/// \return Returns true if there's a collision
	bool checkAABB(const btVector3 &Amin, const btVector3 &Amax, const btVector3 &Bmin, const btVector3 &Bmax);
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Protected Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Shared Pointer to the Model Manager
	std::shared_ptr<asset::ModelManager> m_modelManager;
	/// \brief Unique Pointer to the Ball
	std::unique_ptr<Ball> m_ball;
	/// \brief Unique Pointer to the Goal
	std::unique_ptr<Goal> m_goal;
	/// \brief Boolean to the level state
	bool m_levelComplete;

private:
	// Private Variables
	
};

#endif // __BASELEVEL__