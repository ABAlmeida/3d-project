//----------------------------------------------------------------------------------------------------------------------
/// \file CameraManager.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The main Camera class for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __CAMERAMANAGER__
#define __CAMERAMANAGER__

#pragma once
#include <memory>
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"

namespace asset
{

//----------------------------------------------------------------------------------------------------------------------
/// \name Camera Structure
/// \brief Structure to hold all the Camera Elements
//----------------------------------------------------------------------------------------------------------------------
struct camera
{
	/// \brief Default constructor
	camera() {}
	/// \brief Contructor to set all default values
	camera(glm::vec3 e, glm::vec3 c, glm::vec3 u) : eye(e), center(c), up(u) {}
	
	/// \brief Vec3 Camera Eye Position
	glm::vec3 eye;
	/// \brief Vec3 Camera LookAt direction
	glm::vec3 center;
	/// \brief Vec3 Camera Up direction
	glm::vec3 up;
};

//----------------------------------------------------------------------------------------------------------------------
/// \name Camera Manager Class
//----------------------------------------------------------------------------------------------------------------------
class CameraManager
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Default Constructor
	CameraManager();
	/// \brief Better Class Constructor
	/// \param Const int Screen Width
	/// \param Const int Screen Height
	CameraManager(const int w, const int h);
	/// \brief Class Destructor
	~CameraManager();
	/// \brief Make our Model View Projection Matrix
	/// \param Const matrix4x4 reference to Model Matrix
	/// \return Returns reference to mat4v4 MVP
	glm::mat4& makeMVP(const glm::mat4 &model);
	/// \brief Set the Camera Position
	/// \param Const vec3 reference to Eye
	/// \param Const vec3 reference to Center
	void setCamera(const glm::vec3 &eye, const glm::vec3 &center);
	/// \brief Modify the Camera Position
	/// \param Const vec3 reference to modify camera by
	void modifyCamera(const glm::vec3 &modify);
	/// \brief Set the Eye position of the camera
	/// \param Const vec3 reference to eye position
	void setEye(const glm::vec3 &eye);
	/// \brief Modify the Eye position of the camera
	/// \param Const vec3 reference to modify by
	void modifyEye(const glm::vec3 &modify);
	/// \brief Set the Camera LookAt direction
	/// \param Const vec3 reference to LookAt direction
	void setCenter(const glm::vec3 &center);
	/// \brief Modify the camera LookAt Direction
	/// \param Const vec3 refence to modify by
	void modifyCenter(const glm::vec3 &modify);
	/// \brief Get the camera eye position
	/// \return Returns vec3 camera eye
	inline glm::vec3 getEye() const
	{ return m_camera.eye; }
	/// \brief Get the address of the Model View Projection Matrix
	/// \return Returns the address of mat4v4 matrix
	inline glm::mat4 *getMVP() const
	{ return m_mvp.get(); }
	/// \brief Get the View Orthographic Matrix
	/// \return Returns a mat4x4 matrix
	inline glm::mat4 getVO() const
	{ return *m_VO; }

private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Our class camera
	camera m_camera;

	/// \brief Unique pointer to View Orthographic Matrix4x4
	std::unique_ptr<glm::mat4> m_VO;
	/// \brief Unique Pointer to View Matrix4x4
	std::unique_ptr<glm::mat4> m_view;
	/// \brief Unique Pointer to Projection Matrix4x4
	std::unique_ptr<glm::mat4> m_projection;
	/// \brief Unique Pointer to Model View Projection Matrix4x4
	std::unique_ptr<glm::mat4> m_mvp;
};

}

#endif // __CAMERAMANAGER__