//----------------------------------------------------------------------------------------------------------------------
/// \file Goal.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The Class for the game object , Goal, inheriting for Actor.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __GOAL__
#define __GOAL__

#pragma once
#include "Actor.h"

//----------------------------------------------------------------------------------------------------------------------
/// \name Goal Class
/// \brief Inherits from Actor
//----------------------------------------------------------------------------------------------------------------------
class Goal : public Actor
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	Goal();
	/// \brief Class Destructor
	~Goal();
	/// \brief Initialize call
	/// \param Pointer const to the Model Manager
	/// \param Pointer const to the bullet discrete dynamics world
	void Initialise(std::shared_ptr<asset::ModelManager> &mManager, btDiscreteDynamicsWorld* const dynamicsWorld);
	/// \brief Update Call
	void Update(float dt);
	/// \brief Draw Call
	void Draw();
	/// \brief Get the Physics translation
	void getTrans();
	/// \brief Set the Physics Translation
	void setTrans(const glm::mat4 &newTrans);
	/// \brief Checks AABB between the Vectors
	/// \param Vec3 reference to the min
	/// \param Vec3 reference to the max
	void getAABB(btVector3 &min, btVector3 &max);
private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Posts Physics Data
	std::unique_ptr<btRigidBody> m_postsRB;
	/// \brief Goal Physics Data
	std::unique_ptr<btRigidBody> m_goalRB;
};

#endif // __GOAL__