//----------------------------------------------------------------------------------------------------------------------
/// \file Graphics.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief Initalizes OpenGL and SDL graphics for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __GRAPHICS__
#define __GRAPHICS__

#pragma once
#include "gl/glew.h"
#include <SDL.h>
#include <memory>
#include <iostream>

//----------------------------------------------------------------------------------------------------------------------
/// \name Graphics Class
//----------------------------------------------------------------------------------------------------------------------
class Graphics
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	/// \param Const Int Width
	/// \param Const Int Height
	Graphics(const int width, const int height);
	/// \brief Class Destructor
	~Graphics();
	/// \brief Render Call called at the end of all Rendering
	void Render();
	/// \brief Render3D Call called before all 3D Draws
	void Render3D();
	/// \brief RenderUI Call called before all UI Draws
	void RenderUI();

private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Unique Pointer to SDL Window
	SDL_Window *m_window;
	/// \brief SDL_GLContext for rendering
	SDL_GLContext m_context;
};

#endif // __GRAPHICS__