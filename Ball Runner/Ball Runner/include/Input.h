//----------------------------------------------------------------------------------------------------------------------
/// \file Input.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The main input class for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __INPUT__
#define __INPUT__

#pragma once
#include <SDL.h>

namespace input
{

//----------------------------------------------------------------------------------------------------------------------
/// \name Enum InputValue
/// \brief The stored values for inputs
//----------------------------------------------------------------------------------------------------------------------
enum inputValue
{
	//Exit
	kEsc = 0,
	//Keyboard Controls
	kLArrow, kRArrow, kUArrow, kDArrow,
	kSpace, kEnter,
	// Other Keyboard Keys
	kP,

	//Mouse Controls
	kLMouse, kRMouse, kMMouse,
};

//----------------------------------------------------------------------------------------------------------------------
/// \name InputReader Class
/// \brief Class to handle all Inputs for the engine
//----------------------------------------------------------------------------------------------------------------------
class inputReader
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	
	/// \brief Class Constructor
	inputReader();
	/// \brief Class Destructor
	~inputReader();

	/// \brief Reads the current Input
	/// \param SDL_Event to get the Input
	/// \return Enum InputValue as Int
	int readInput(SDL_Event &incomingEvent);
};

}

#endif // __INPUT__