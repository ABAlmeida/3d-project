//----------------------------------------------------------------------------------------------------------------------
/// \file Level1.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The first level for the game.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __LEVEL1__
#define __LEVEL1__

#pragma once
#include "glm\glm.hpp"
#include "BaseLevel.h"
//----------------------------------------------------------------------------------------------------------------------
/// \name Fowards Declarations
/// \brief All classes forward declared here
//----------------------------------------------------------------------------------------------------------------------
/// \brief Map Declaration
class Map;
//----------------------------------------------------------------------------------------------------------------------
/// \name Level 1 Class
/// \brief Publicly inherits from Base Level
//----------------------------------------------------------------------------------------------------------------------
class Level1 : public BaseLevel
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	Level1();
	/// \brief Class Destructor
	~Level1();
	/// \brief Virtual Initialise Override
	/// \param Standard Shared Pointer reference to Model Manager
	/// \param Pointer const to the Dynamics World
	void onInitialise(std::shared_ptr<asset::ModelManager> &mManager, btDiscreteDynamicsWorld* const dynamicsWorld);
	/// \brief Virtual Update Override
	/// \param Const float Delta Time
	void onUpdate(const float dt);
	/// \brief Virtual Draw Override
	void onDraw();
	/// \brief Virtual UI Draw Override
	void onGUI();
private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Unique Pointer to Map
	std::unique_ptr<Map> m_map;
	/// \brief Vec3 Last Ball Postion
	glm::vec3 lastBallpos;
};

#endif // __LEVEL1__