//----------------------------------------------------------------------------------------------------------------------
/// \file LightingProgram.h
/// \author Antonio Almeida
/// \date 07/06/2015
/// \brief The lighting shader program for 3D rendering for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __LightingProgram__
#define __LightingProgram__

#pragma once
#include "ShaderProgram.h"

//----------------------------------------------------------------------------------------------------------------------
/// \name Fowards Declarations
/// \brief All classes forward declared here
//----------------------------------------------------------------------------------------------------------------------
/// \brief Model Declaration
namespace asset { class model; }

namespace shader
{

//----------------------------------------------------------------------------------------------------------------------
/// \name Lighting Program Class
/// \brief Publicly inherits from the Shader Program Class
//----------------------------------------------------------------------------------------------------------------------
class LightingProgram : public ShaderProgram
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	LightingProgram();
	/// \brief Class Destructor
	~LightingProgram();
	/// \brief Initialises the Lighting Program
	void initialiseProgram();
	/// \brief Passes in the Values to the Shaders
	/// \param Const mat4x4 reference to the Model Matrix
	/// \param Const mat4x4 reference to the Model View Projection Matrix
	/// \param Const asset::model to the Model Data
	/// \param Const vec3 reference to the Camera Position
	void useUniformValues(const glm::mat4 &mMatrix, const glm::mat4 &mvpMatrix, const asset::model &m, const glm::vec3 &eye);
private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Gets the Uniform Slots for the Shaders
	void getUniformSlots();

	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief GLint Model View Projection Matrix Slot
	GLint m_mvpSlot;
	/// \brief GLint Model  Matrix Slot
	GLint m_mSlot;
	/// \brief GLint Texture Slot
	GLint m_textureSlot;
	/// \brief GLint Ambient Lighting Colour Slot
	GLint m_aColourSlot;
	/// \brief GLint Ambient Lighting Intensity Slot
	GLint m_aIntensitySlot;
	/// \brief GLint Diffuse Lighting Direction Slot
	GLint m_dDirectionSlot;
	/// \brief GLint Diffuse Lighting Intensity Slot
	GLint m_dIntensitySlot;
	/// \brief GLint Camera Position Slot
	GLint m_eyeSlot;
	/// \brief GLint Specular Lighting Power Slot
	GLint m_sPowerSlot;
	/// \brief GLint Specular Lighting Intensity Slot
	GLint m_sIntensitySlot;
};

}

#endif // __LightingProgram__