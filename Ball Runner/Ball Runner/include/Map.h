//----------------------------------------------------------------------------------------------------------------------
/// \file Map.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The Class for the game object , Map, inheriting for Actor.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __MAP__
#define __MAP__

#pragma once
#include <SDL.h>
#include "Actor.h"

//----------------------------------------------------------------------------------------------------------------------
/// \name Map Class
/// \brief Inherits from Actor
//----------------------------------------------------------------------------------------------------------------------
class Map : public Actor
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	Map();
	/// \class Destructor
	~Map();
	/// \brief Initialize call
	void Initialise(std::shared_ptr<asset::ModelManager> &mManager, btDiscreteDynamicsWorld* const dynamicsWorld);
	/// \brief Update Call
	void Update(float dt);
	/// \brief Draw Call
	void Draw();
	/// \brief Modify the map based upon the ball pos/play input
	/// \param Vec3 value to the ball position
	void modifyMap(glm::vec3 ball_pos);
	/// \brief Reset the Map
	void resetMap();
	/// \brief Give the Transform Matrix to the Physics Engine
	void setTrans();
	/// \brief Get the transformation for the Goal
	/// \return Returns a mat4x4
	glm::mat4 getTrans();
	/// \brief Set the mouse position in the class
	void setMouse()
	{ SDL_GetMouseState(&mouse_x, &mouse_y); }
private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Local Transform data
	glm::mat4 m_localMatrix;
	/// \brief Int Mouse X Data
	int mouse_x;
	/// \brief Int Mouse Y Data
	int mouse_y;
	/// \brief Physics Data
	std::unique_ptr<btRigidBody> m_RigidBody;
};

#endif // __MAP__