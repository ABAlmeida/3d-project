//----------------------------------------------------------------------------------------------------------------------
/// \file Menu.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The main menu class for the system.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __MENU__
#define __MENU__

#pragma once
#include "MiniApp.h"
#include <memory>
//----------------------------------------------------------------------------------------------------------------------
/// \name Fowards Declarations
/// \brief All classes forward declared here
//----------------------------------------------------------------------------------------------------------------------
/// \brief Model Manager Declaration
namespace asset { class ModelManager; }
/// \brief Sprite Declaration
class Sprite;
//----------------------------------------------------------------------------------------------------------------------
/// \name Menu Class
/// \brief Inherits from the MiniApp Class
//----------------------------------------------------------------------------------------------------------------------
class Menu : public MiniApp
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	Menu();
	/// \brief Class Destructor
	~Menu();
	/// \brief Initialise override
	void onInitialise(std::shared_ptr<asset::ModelManager> &mManager);
	/// \brief Input override
	int onInput(const int input);
	/// \brief Update override
	void onUpdate(const float dt);
	/// \brief Draw override
	void onDraw();
	/// \brief GUI override
	void onGUI();
private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Backdrop Sprite
	std::unique_ptr<Sprite> m_backdrop;
	/// \brief Play Sprite
	std::unique_ptr<Sprite> m_play;
	/// \brief On Play Down Sprite
	std::unique_ptr<Sprite> m_playdown;
	/// \brief Quit Sprite
	std::unique_ptr<Sprite> m_quit;
	/// \brief On Quit Down Sprite
	std::unique_ptr<Sprite> m_quitdown;
	/// \brief Declare our selected sprite
	int m_selected;
};

#endif // __MENU__