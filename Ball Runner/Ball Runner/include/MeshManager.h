//----------------------------------------------------------------------------------------------------------------------
/// \file MeshManager.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief This class handles loading mesh data for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __MESHMANAGER__
#define __MESHMANAGER__

#pragma once
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include "gl/glew.h"
#include "glm/glm.hpp"

namespace asset
{

//----------------------------------------------------------------------------------------------------------------------
/// \name Vertex Structure
/// \brief Our structure for holding all the parts of a Vertex
//----------------------------------------------------------------------------------------------------------------------
struct vertex
{
	/// \brief Vertex Vec3 Position Data
	glm::vec3 pos;
	/// \brief Vertex Vec2 Texture Data
	glm::vec2 uv;
	/// \brief Vertex Vec3 Normal Data
	glm::vec3 normal;
};

//----------------------------------------------------------------------------------------------------------------------
/// \name Mesh Data Structure
/// \brief Our structure for holding all our mesh data
//----------------------------------------------------------------------------------------------------------------------
struct mData
{
	/// \brief Standard Vector containing all Vertex Data
	std::vector<vertex> vertices;
	/// \brief Standard Vector containing all Index Data
	std::vector<unsigned int> indices;
};

//----------------------------------------------------------------------------------------------------------------------
/// \ MeshManager Class
//----------------------------------------------------------------------------------------------------------------------
class MeshManager
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	MeshManager();
	/// \brief Class Destructor
	~MeshManager();

	/// \brief Load the mesh return the index
	/// \param Const Standard String Reference containing File Name
	/// \return Returns Index containing Mesh Data Position
	int loadModel(const std::string &file);
	/// \brief Load the mesh components into OpenGL using the Index
	/// \param Const Int Mesh Data, Index Position
	/// \return Returns True if successfully loaded
	bool loadModelComponents(const int i);

	/// \brief Gets the VBO of the current Mesh Data
	/// \return GLuint for the VBO
	inline GLuint getVBO() const
	{ return m_VBO; }
	/// \brief Gets the IBO of the current Mesh Data
	/// \return GLuint for the IBO
	inline GLuint getIBO() const
	{ return m_IBO; }
	/// \brief Gets the number of Indices of the current Mesh Data
	/// \param Const Int Mesh Data, Index Position
	/// \return Unsigned int containing number of Indices
	inline int getIndexCount(const int i) const
	{ return (m_models[i].indices.size()); }

private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Loads the Mesh Data into the class
	/// \param Const Standard String Reference containing File Name
	/// \param mData reference to load the contents into
	/// \return Returns boolean true if succeeds
	bool loadFromMesh(const std::string &file, mData &components);
	/// \brief Loads the data into a string from file
	/// \param Const Standard String Reference containing File Name
	/// \param Standard String Reference containing file data
	void loadString(const std::string &file, std::string &returnData) const;

	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Standard Vector of Meshes
	std::vector<mData> m_models;

	/// \brief Currently Loaded VBO
	GLuint m_VBO;
	/// \brief Currently Loaded IBO
	GLuint m_IBO;
};

}

#endif // __MESHMANAGER__