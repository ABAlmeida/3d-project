//----------------------------------------------------------------------------------------------------------------------
/// \file MiniApp.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The base class for all MiniApp's within the engine to inherit from.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __MINIAPP__
#define __MINIAPP__

#pragma once

//----------------------------------------------------------------------------------------------------------------------
/// \name Base MiniApp Class
//----------------------------------------------------------------------------------------------------------------------
class MiniApp
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Member Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	MiniApp() {};
	/// \brief Class Destructor
	virtual ~MiniApp() {};
	/// \brief Pure Virtual Input Call
	virtual int onInput(const int input) = 0;
	/// \brief Pure Virtula Update Call
	virtual void onUpdate(const float dt) = 0;
	/// \brief Pure Virtual Draw Call
	virtual void onDraw() = 0;
	/// \brief Pure Virtual UI Draw Call
	virtual void onGUI() = 0;
};

#endif // __MINIAPP__