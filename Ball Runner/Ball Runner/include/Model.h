//----------------------------------------------------------------------------------------------------------------------
/// \file Model.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The model and sprite classes for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __MODEL__
#define __MODEL__

#pragma once
#include "gl\glew.h"
#include "glm\glm.hpp"

namespace asset
{

//----------------------------------------------------------------------------------------------------------------------
/// \name Sprite Structure
/// \brief Holds all the components needed for drawing sprites
//----------------------------------------------------------------------------------------------------------------------
struct UI
{
	/// \brief GLuint Vertex Buffer Object
	GLuint VBO;
	/// \brief GLuint Index Buffer Object
	GLuint IBO;
	/// \brief Uint Indices Count
	unsigned int indexCount;
	/// \brief GLuint Texture Buffer Object
	GLuint TBO;
	/// \brief int Width
	int width;
	/// \brief int Height
	int height;
};

//----------------------------------------------------------------------------------------------------------------------
/// \name Model Class
/// \brief Holds all the components needed for drawing models
//----------------------------------------------------------------------------------------------------------------------
class model
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	model();
	/// \brief Class Destructor
	~model();
	
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Variables
	//----------------------------------------------------------------------------------------------------------------------

	/// \brief GLuint Vertex Buffer Object
	GLuint VBO;
	/// \brief GLuint Index Buffer Object
	GLuint IBO;
	/// \brief Uint Indices count
	unsigned int indexCount;
	/// \brief GLuint Texture Buffer Object
	GLuint TBO;
	/// \brief GLuint Texture Target
	GLenum TT;

	/// \brief Vec3 Ambient Lighting Colour
	glm::vec3 aColour;
	/// \brief Float Ambient Lighting Intensity
	float aIntensity;
	/// \brief Vec3 Diffuse Lighting Direction
	glm::vec3 dDirection;
	/// \brief Float Diffuse Lighting Intensity
	float dIntensity;
	/// \brief Float Specular Lighting Power
	float sPower;
	/// \brief Float Specular Lighting Intensity
	float sIntensity;
};

}

#endif // __MODEL__