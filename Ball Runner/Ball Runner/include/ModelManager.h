//----------------------------------------------------------------------------------------------------------------------
/// \file ModelManager.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief Encapsulation class for most of the Graphics for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __MODELMANAGER__
#define __MODELMANAGER__

#pragma once
#include "gl\glew.h"
#include "glm\glm.hpp"
#include <memory>

//----------------------------------------------------------------------------------------------------------------------
/// \name Fowards Declarations
/// \brief All classes forward declared here
//----------------------------------------------------------------------------------------------------------------------
/// \brief Model Declaration
namespace asset { class model; }
/// \brief UI Declaration
namespace asset { struct UI; }
/// \brief MeshManager Declaration
namespace asset { class MeshManager; }
/// \brief ShaderManager Declaration
namespace shader { class ShaderManager; }
/// \brief CameraManager Declaration
namespace asset { class CameraManager; }
/// \brief TextureManager Declaration
namespace asset { class TextureManager; }

namespace asset
{

//----------------------------------------------------------------------------------------------------------------------
/// \name ModelManager Class
//----------------------------------------------------------------------------------------------------------------------
class ModelManager
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	ModelManager();
	/// \brief Class Destructor
	~ModelManager();

	/// \brief Load in the Mesh Data into the Model
	/// \param Reference to the Model
	/// \param Const char pointer const to the file path
	void loadMesh(model &m, const char* const meshpath);
	/// \brief Load in the Texture Data into the Model
	/// \param Reference to the Model
	/// \param Const char pointer const to the file path
	/// \param Const GLenum to the Texture Target
	void loadTexture(model &m, const char* const texturepath, const GLenum textureTarget);
	/// \brief Load in the Sprite Data into the Sprite
	/// \param Reference to the sprite
	/// \param Const char pointer const to the file path
	void loadSprite(UI &ui, const char* const spritePath);
	/// \brief Draw call for the Models
	/// \param Const reference to the Model
	/// \param Const reference to the Model Matrix4x4
	void drawModel(const model &m, const glm::mat4 &mMatrix);
	/// \brief Draw call for the Sprites
	/// \param Const reference to the Sprite
	/// \param Const reference to the Sprite Matrix4x4
	void drawUI(const UI &ui, const glm::mat4 &mMatrix);
	/// \brief Set the Camera Properties
	/// \param Const vec3 reference to the Camera Postion
	/// \param Const vec3 reference to the Camera LookAt
	void setCamera(const glm::vec3 &eye, const glm::vec3 &center);
	/// \brief Modify the Camera Properties
	/// \param Const vec3 reference to the Camera Postion
	/// \param Const vec3 reference to the Camera LookAt
	void modifyCamera(const glm::vec3 &eye, const glm::vec3 &center);
private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \ Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \ Unique Pointer to the Mesh Manager
	std::unique_ptr<MeshManager> m_meshManager;
	/// \ Unique Pointer to the Texture Manager
	std::unique_ptr<TextureManager> m_textureManager;
	/// \ Unique Pointer to the Shader Manager
	std::unique_ptr<shader::ShaderManager> m_shaderManager;
	/// \ Unique Pointer to the Camera Manager
	std::unique_ptr<CameraManager> m_cameraManager;

	/// \brief GLuint Sprite Vertex Buffer Object
	GLuint m_spriteVBO;
	/// \brief GLuint Sprite Index Buffer Object
	GLuint m_spriteIBO;
	/// \brief Unsigned int to the Sprite indicex count
	unsigned int m_spriteCount;

	/// \brief 3D Shader Holder
	int m_3dShader;
	/// \brief 2D Shader Holder
	int m_2dShader;
};

}

#endif // __MODELMANAGER__