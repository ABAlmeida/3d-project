//----------------------------------------------------------------------------------------------------------------------
/// \file Physics.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The main Physics using bullet for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __PHYSICS__
#define __PHYSICS__

#pragma once
#include <btBulletDynamicsCommon.h>
#include <memory>

//----------------------------------------------------------------------------------------------------------------------
/// \name Physics Class
//----------------------------------------------------------------------------------------------------------------------
class bPhysics
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \ Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	bPhysics();
	/// \brief Class Destructor
	~bPhysics();

	/// \brief Step the simulation for the Physics World
	/// \param float delta time
	inline void stepWorld(float dt)
	{ m_dynamicsWorld->stepSimulation(dt, 1, 0.016666f); }
	/// \brief Returns the Physics world so Actors can be added
	/// \return Pointer to Dynamics World
	inline btDiscreteDynamicsWorld *getWorld()
	{ return m_dynamicsWorld.get(); }
private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
	std::unique_ptr<btDefaultCollisionConfiguration> m_config;
	/// \brief use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
	std::unique_ptr<btCollisionDispatcher> m_dispatcher;
	/// \brief btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
	std::unique_ptr<btBroadphaseInterface> m_oPC;
	/// \brief the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
	std::unique_ptr<btSequentialImpulseConstraintSolver> m_solver;
	/// \brief the dynamics world used for storing and computing all actors
	std::shared_ptr<btDiscreteDynamicsWorld> m_dynamicsWorld;
};

#endif // __PHYSICS__