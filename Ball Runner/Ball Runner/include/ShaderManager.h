//----------------------------------------------------------------------------------------------------------------------
/// \file ShaderManager.h
/// \author Antonio Almeida
/// \date 07/06/2015
/// \brief The encapsulation class for all shader handling for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __SHADERMANAGER__
#define __SHADERMANAGER__

#pragma once
#include "glm\glm.hpp"
#include <memory>

//----------------------------------------------------------------------------------------------------------------------
/// \name Fowards Declarations
/// \brief All classes forward declared here
//----------------------------------------------------------------------------------------------------------------------
/// \brief Model Declaration
namespace asset { class model; }
/// \brief UIProgram Declaration
namespace shader { class UIProgram; }
/// \brief LightingProgram Declaration
namespace shader { class LightingProgram; }

namespace shader
{

//----------------------------------------------------------------------------------------------------------------------
/// \name ShaderManager Class
//----------------------------------------------------------------------------------------------------------------------
class ShaderManager
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	ShaderManager();
	/// \brief Class Destructor
	~ShaderManager();

	/// \brief Use the Lighting Program
	/// \param Const mat4x4 refernece to the Model Matrix
	/// \param Const mat4x4 reference to the Model View Project Matrix
	/// \param Const reference to the asset::model
	/// \param Const vec3 reference to the camera position
	/// \return Returns boolean true if successful
	bool useLightingProgram(const glm::mat4 &mMatrix, const glm::mat4 &mvpMatrix, const asset::model &m, const glm::vec3 &eye);
	/// \brief Use the UI Program
	/// \param Const mat4x4 reference to the Model View Projection Matrix
	/// \return Returns boolean true if successful
	bool useUIProgram(const glm::mat4 &mvpMatrix);

private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Unique Pointer to the Lighting Program
	std::unique_ptr<LightingProgram> m_lightingProgram;
	std::unique_ptr<UIProgram> m_uiProgram;
};

}

#endif // __SHADERMANAGER__