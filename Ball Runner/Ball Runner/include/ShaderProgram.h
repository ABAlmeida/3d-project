//----------------------------------------------------------------------------------------------------------------------
/// \file ShaderProgram.h
/// \author Antonio Almeida
/// \date 07/06/2015
/// \brief The base class for all Shader Programs for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __SHADERPROGRAM__
#define __SHADERPROGRAM__

#pragma once
#include "gl/glew.h"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include "glm\glm.hpp"
#include "glm\gtc\type_ptr.hpp"

namespace shader
{

//----------------------------------------------------------------------------------------------------------------------
/// \name Base Shader Program Class
//----------------------------------------------------------------------------------------------------------------------
class ShaderProgram
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	ShaderProgram();
	/// \brief Class Destructor
	~ShaderProgram();
	/// \brief Pure Virtual Initialise Program Function
	virtual void initialiseProgram() = 0;
	/// \brief Use the shader program
	/// \return Returns a boolean true if successful
	bool useShaderProgram();
protected:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Protected Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Make the shader program
	/// \return Returns boolean true if successful
	bool makeShaderProgram();
	/// \brief Add our shader program
	/// \return Returns boolean true if successful
	bool addShaderProgram();
	/// \brief Add a shader to our shader program
	/// \param Const char pointer const to the Shader Name
	/// \param Const GLenum to the Shader Type
	/// \return Returns boolean true if successful
	bool addShader(const char* const ShaderName, const GLenum ShaderType);
	/// \brief Uniform function to the the uniform slot
	/// \param GLint reference to the Slot
	/// \param Const char pointer const to the Shader Name
	void getUniform(GLint &slot, const char* const shaderName);

	//----------------------------------------------------------------------------------------------------------------------
	/// \name Protected Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief GLuint to the Shader Program
	GLuint m_shaderProgram;

private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Load our Shader into our Shader program
	/// \param Const char pointer const to the File Name
	/// \param Standard String to the String
	bool loadShader(const char* const charVar, std::string &stringVar);
	
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Our error checking values
	GLint m_Success;
};

}

#endif // __SHADERPROGRAM__