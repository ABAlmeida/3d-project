//----------------------------------------------------------------------------------------------------------------------
/// \file Application.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The main application class for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __SPRITE__
#define __SPRITE__

#pragma once
#include "glm\glm.hpp"
#include <memory>

//----------------------------------------------------------------------------------------------------------------------
/// \name Fowards Declarations
/// \brief All classes forward declared here
//----------------------------------------------------------------------------------------------------------------------
/// \brief UI Declaration
namespace asset { struct UI; }
/// \brief Model Declaration
namespace asset { class model; }
/// \brief Model Manager Declaration
namespace asset { class ModelManager; }

//----------------------------------------------------------------------------------------------------------------------
/// \name Base Sprite Class
//----------------------------------------------------------------------------------------------------------------------
class Sprite
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	/// \param Const pointer to the Model Manager
	/// \param Const char pointer const to the sprite file path
	Sprite(std::shared_ptr<asset::ModelManager> &mManager, const char* const filepath);
	/// \brief Class Destructor
	~Sprite();
	/// \brief Draw call using the Model Manager
	void Draw();
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Vec2 position lof the sprite
	glm::vec2 pos;
protected:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Protected Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Vec3 on the Sprite scale
	glm::vec3 m_scale;
	/// \brief Vec3 on the Sprite rotation
	glm::vec3 m_rotate;
	/// \brief Vec3 on the Sprite Translation
	glm::vec3 m_pos;
	/// \brief Mat4x4 to the Model Matrix
	glm::mat4 m_modelMatrix;
private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Shared Pointer to the Model Manager
	std::shared_ptr<asset::ModelManager> m_modelManager;
	/// \brief Unique Pointer to the Sprite Data
	std::unique_ptr<asset::UI> m_UI;
};

#endif// __SPRITE__