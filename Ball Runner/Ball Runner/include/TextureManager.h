//----------------------------------------------------------------------------------------------------------------------
/// \file TextureManager.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The class for loading Textures for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __TEXTUREMANAGER__
#define __TEXTUREMANAGER__

#pragma once
#include "gl\glew.h"
#include "glm\glm.hpp"
#include <iostream>
#include <memory>
#include <vector>
#include <fstream>
#include <SDL_opengl.h>
#include <SDL.h>
#include <SDL_image.h>

namespace asset
{

//----------------------------------------------------------------------------------------------------------------------
/// \name Texture Data Structure
/// \brief Structure to hold all Texture Data
//----------------------------------------------------------------------------------------------------------------------
struct tData
{
	/// \brief Width of Data
	int width;
	/// \brief Height of Data
	int height;
	/// \brief Number of Bytes per Pixel
	int BytesPerPixel;
	/// \brief Pointer to Pixels
	void* pixels;
	/// \brief Enum to Format
	GLenum format;
};

//----------------------------------------------------------------------------------------------------------------------
/// \name Texture Manager Class
//----------------------------------------------------------------------------------------------------------------------
class TextureManager
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	TextureManager();
	/// \brief Class Destructor
	~TextureManager();

	/// \brief Load our Texture and return it's index
	/// \param Const Char Pointer to File Name
	/// \return Returns the Index the Texture is Loaded into
	int loadTexture(const char *file);
	/// \brief Load our texture data into OpenGL
	/// \param Const index of the Texture
	/// \param Const GLenum Target of the Texture
	bool loadTextureComponents(const int i, const GLenum textureTarget);

	/// \brief Gets the TBO of the current Texture Data
	/// \return GLuint for the TBO
	inline GLuint getTBO() const
	{ return m_TBO; }
	/// \brief Gets the Texture Target of the current Texture Data
	/// \return GLuint for the TT
	inline GLenum getTT() const
	{ return m_TT; }
	/// \brief Gets the VBO of the current Mesh Data
	/// \return GLuint for the VBO
	inline tData getTData() const
	{ return m_textures.back(); }

private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Loads the texture into the class
	/// \param Const char pointer const to the File path
	/// \param tData pointer const to the data
	/// \return Returns boolean true if the image is successfully loaded
	bool loadImage(const char* const FilePath, tData* const data);

	//----------------------------------------------------------------------------------------------------------------------
	/// \ Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief All texture data stored here
	std::vector<tData> m_textures;
	/// \brief GLuint for the Texture Buffer Object
	GLuint m_TBO;
	/// \brief GLuint for the Texture Target
	GLenum m_TT;
};

}

#endif // __TEXTUREMANAGER__