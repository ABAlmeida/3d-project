#ifndef __TRANSFORMMANAGER__
#define __TRANSFORMMANAGER__

#pragma once
#include "glm/glm.hpp"

#define PI 3.14159265359f
#define ToRadian(x) (float)(((x) * PI / 180.0f))

// Maker individual matrices
static glm::mat4 makeScaleMatrix(const glm::vec3 &scale)
{
	// Create our Scale Matrix
	glm::mat4 ret = glm::mat4(0.0f);
	ret[0][0] = scale.x;
	ret[1][1] = scale.y;
	ret[2][2] = scale.z;
	ret[3][3] = 1.0f;

	// Return it
	return ret;
}

static glm::mat4 makeRotationMatrix(const glm::vec3 &rotate)
{
	// Create our rotation matrices
	glm::mat4 rx, ry, rz, ret;

	// Convert our rotation values to radians
	const float x = ToRadian(rotate.x);
	const float y = ToRadian(rotate.y);
	const float z = ToRadian(rotate.z);

	// Create our rotation around the x axis matrix
	rx[0][0] = 1.0f; rx[0][1] = 0.0f;	 rx[0][2] = 0.0f;	  rx[0][3] = 0.0f;
	rx[1][0] = 0.0f; rx[1][1] = cosf(x); rx[1][2] = sinf(x); rx[1][3] = 0.0f;
	rx[2][0] = 0.0f; rx[2][1] = -sinf(x); rx[2][2] = cosf(x);  rx[2][3] = 0.0f;
	rx[3][0] = 0.0f; rx[3][1] = 0.0f;	 rx[3][2] = 0.0f;	  rx[3][3] = 1.0f;

	// Create our rotation around the y axis matrix
	ry[0][0] = cosf(y); ry[0][1] = 0.0f; ry[0][2] = sinf(y); ry[0][3] = 0.0f;
	ry[1][0] = 0.0f;	ry[1][1] = 1.0f; ry[1][2] = 0.0f;	  ry[1][3] = 0.0f;
	ry[2][0] = -sinf(y); ry[2][1] = 0.0f; ry[2][2] = cosf(y);  ry[2][3] = 0.0f;
	ry[3][0] = 0.0f;	ry[3][1] = 0.0f; ry[3][2] = 0.0f;	  ry[3][3] = 1.0f;

	// Create our rotation around the z axis matrix
	rz[0][0] = cosf(z); rz[0][1] = sinf(z); rz[0][2] = 0.0f; rz[0][3] = 0.0f;
	rz[1][0] = -sinf(z); rz[1][1] = cosf(z);	 rz[1][2] = 0.0f; rz[1][3] = 0.0f;
	rz[2][0] = 0.0f;	rz[2][1] = 0.0f;	 rz[2][2] = 1.0f; rz[2][3] = 0.0f;
	rz[3][0] = 0.0f;	rz[3][1] = 0.0f;	 rz[3][2] = 0.0f; rz[3][3] = 1.0f;

	// Multiple our matrices together
	ret = rz * ry * rx;
	// Return our rotation matrix
	return ret;
}

static glm::mat4 makeTranslationMatrix(const glm::vec3 &translate)
{
	// Create our translation matrix
	glm::mat4 ret = glm::mat4(0.0f);
	ret[0][0] = 1.0f;
	ret[3][0] = translate.x; //X
	ret[1][1] = 1.0f;
	ret[3][1] = translate.y; //Y
	ret[2][2] = 1.0f;
	ret[3][2] = translate.z; //Z
	ret[3][3] = 1.0f;

	// Return it
	return ret;
}

// Make combined Matrix
static glm::mat4 makeMatrix(const glm::vec3 &scale, const glm::vec3 &rotate, const glm::vec3 &translate)
{
	// Get our individual matrices
	glm::mat4 scaleMatrix = makeScaleMatrix(scale);
	glm::mat4 rotateMatrix = makeRotationMatrix(rotate);
	glm::mat4 translateMatrix = makeTranslationMatrix(translate);

	// Multiply them together Scale -> Rotate -> Translate for the correct outcome
	glm::mat4 ret = translateMatrix * rotateMatrix * scaleMatrix;
	// Return it
	return ret;
}


#endif // __TRANSFORMMANAGER__