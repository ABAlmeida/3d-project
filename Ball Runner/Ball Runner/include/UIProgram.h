//----------------------------------------------------------------------------------------------------------------------
/// \file UIProgram.h
/// \author Antonio Almeida
/// \date 07/06/2015
/// \brief The UI shader program for 2D rendering for the engine.
//----------------------------------------------------------------------------------------------------------------------
#ifndef __UIPROGRAM__
#define __UIPROGRAM__

#pragma once
#include "ShaderProgram.h"

namespace shader
{

//----------------------------------------------------------------------------------------------------------------------
/// \name UI Program Class
/// \brief Publicly inherits from the Shader Program Class
//----------------------------------------------------------------------------------------------------------------------
class UIProgram : public ShaderProgram
{
public:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Public Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Class Constructor
	UIProgram();
	/// \brief Class Destructor
	~UIProgram();
	/// \brief Initialise the UI Shader Program
	void initialiseProgram();
	/// \brief Pass our Values through the Uniforms to the Shaders
	/// \param Const mat4x4 reference to the Model View Projection Matrix
	void useUniformValues(const glm::mat4 &mvpMatrix);
private:
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Functions
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief Get the Uniform slots for the Shader values
	void getUniformSlots();
	
	//----------------------------------------------------------------------------------------------------------------------
	/// \name Private Class Variables
	//----------------------------------------------------------------------------------------------------------------------
	/// \brief GLint Model View Projection Matrix Slot
	GLint m_mvpSlot;
	/// \brief GLint Texture Slot
	GLint m_textureSlot;
};

}

#endif // __UIPROGRAM__