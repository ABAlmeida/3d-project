// Specifies the Version of OpenGL we want to use
#version 330

// Input colour in the Fragment Shader
// Undergoes interpolation by the Rasterizer
in vec2 f_UV;

// Declares we are outputting a colour
out vec4 FragColor;

// Sampler for the Texture
uniform sampler2D Sampler;

void main()
{   
    // Set the fragment to be the textured pixel
    FragColor = texture2D(Sampler, f_UV.xy);
}
