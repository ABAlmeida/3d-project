// Specifies the Version of OpenGL we want to use
#version 330

// Input colour in the Fragment Shader
// Undergoes interpolation by the Rasterizer
in vec2 f_UV;
in vec3 f_Normal;
in vec3 f_ModelPos;

// Declares we are outputting a colour
out vec4 FragColor;

// Sampler for the Texture
uniform sampler2D Sampler;
uniform vec3 f_aColour;
uniform float f_aIntensity;
uniform vec3 f_dDirection;
uniform float f_dIntensity;
uniform vec3 f_eye;
uniform float f_sPower;
uniform float f_sIntensity;

void main()
{   
    vec4 ambientColour = vec4(f_aColour, 1.0f) * f_aIntensity;
    vec3 lightDir = -f_dDirection;
    vec3 normal = normalize(f_Normal);
   
    float diffuseFactor = dot(normal, lightDir);
   
    vec4 diffuseColour = vec4(0, 0, 0, 0);
    vec4 specularColour = vec4(0, 0, 0, 0);
   
    if (diffuseFactor > 0)
    {
        diffuseColour = vec4(f_aColour, 1.0f) * f_dIntensity * diffuseFactor;
        vec3 vertexToEye = normalize(f_eye - f_ModelPos);
        vec3 lightRef = normalize(reflect(f_dDirection, normal));
        float specularFactor = dot(vertexToEye, lightRef);
        specularFactor = pow(specularFactor, f_sPower);
        if (specularFactor > 0)
        {
            specularColour = vec4(f_aColour, 1.0f) * f_sIntensity * specularFactor;
        }
    }
    // Set the fragment to be the textured pixel
    FragColor = texture2D(Sampler, f_UV.xy) * (ambientColour + diffuseColour + specularColour);
}
