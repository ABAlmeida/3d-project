//Specifies the version of OpenGL we are using
#version 330

//Specifies that the Vertex will have 3 components to determine its position
layout (location = 0) in vec3 v_Position;
layout (location = 1) in vec2 v_UV;
layout (location = 2) in vec3 v_Normal;

//Receive our Transform Matrix
uniform mat4 v_MVP;
uniform mat4 v_Model;

//Passed between pipeline stages declared with OUT
out vec2 f_UV;
out vec3 f_Normal;
out vec3 f_ModelPos;


//Modify the position of the Vertices as necessary
void main()
{	
	//Multiply the position by the MVP matrix
	gl_Position = v_MVP * vec4(v_Position, 1.0f);

	f_UV = v_UV;
	f_Normal = (v_Model * vec4(v_Normal, 0.0f)).xyz;
	f_ModelPos = (v_Model * vec4(v_Position.xyz, 1.0f)).xyz;
}