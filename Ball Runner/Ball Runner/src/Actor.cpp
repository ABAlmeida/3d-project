#include "Actor.h"
#include "Model.h"
#include "ModelManager.h"
#include "TransformManager.h"
#include <btBulletDynamicsCommon.h>

Actor::Actor() : m_scale(glm::vec3(1.0f, 1.0f, 1.0f)),
				m_rotate(glm::vec3(0.0f, 0.0f, 0.0f)),
				m_pos(glm::vec3(0.0f, 0.0f, 0.0f)),
				m_modelMatrix(makeMatrix(m_scale, m_rotate, m_pos)),
				m_model(new asset::model()),
				m_modelManager()
{

}

Actor::~Actor()
{
	// Clean up our mess
}

bool Actor::Setup(std::shared_ptr<asset::ModelManager> &mManager, const char* meshpath, const char* texturepath, GLenum textureTarget)
{
	if (mManager == NULL)
	{
		return false;
	}
	
	// Point our manager to the Model Manager
	m_modelManager = mManager;
	// Load in our Model
	m_modelManager->loadMesh(*m_model, meshpath);
	m_modelManager->loadTexture(*m_model, texturepath, textureTarget);

	return true;
}

void Actor::UpdateActor()
{
	// Update Call
	m_modelMatrix = makeMatrix(m_scale, m_rotate, m_pos);
}

void Actor::Draw3D()
{
	// Pass our Model data to our Draw Function
	m_modelManager->drawModel(*m_model, m_modelMatrix);
}