#pragma once
#include "Application.h"
#include "Graphics.h"
#include "Input.h"
#include "Physics.h"
#include "ModelManager.h"
#include "Menu.h"
#include "Level1.h"

App::App()
{
	
}

App::~App()
{

}

void App::onInitialise(const int w, const int h)
{
	// Initialise all member pointers
	m_graphics.reset(new Graphics(w, h));
	m_input.reset(new input::inputReader());
	m_modelManager.reset(new asset::ModelManager());
	m_physics = new bPhysics();
	// Intialise our Mini Apps
	m_menu.reset(new Menu());
	m_menu->onInitialise(m_modelManager);
	m_level1.reset(new Level1());
	m_level1->onInitialise(m_modelManager, m_physics->getWorld());

	// Initialise our Variables
	m_ActiveApp = m_menu;
	m_dt = 0;
	m_last = 0;
	m_mode = -1;
	m_inputEvent = -1;
}

bool App::onInput()
{
	// Retrieve the key that has been pressed
	m_inputEvent = m_input->readInput(m_event);
	// Pass it into the active 
	m_mode = m_ActiveApp->onInput(m_inputEvent);
	// Check which state the Active Application should be in
	switch (m_mode)
	{
	case -1:
		break;
	case 1:
		m_ActiveApp = m_menu;
		break;
	case 2:
		m_ActiveApp = m_level1;
		break;
	case 3:
		//m_ActiveApp = m_level1;
		break;
	}
	// Check if the program should close
	if (m_mode == 0)
	{ return false; }

	return true;
}

void App::onUpdate()
{	
	// Retrieve dt
	m_dt = deltaTime();
	// Update the Active App using DeltaTs
	m_ActiveApp->onUpdate(m_dt);
	// Update the physics using DeltaTs
	m_physics->stepWorld(m_dt);
}

void App::onDraw()
{
	// 3D Drawing Set Up, Must be called first
	m_graphics->Render3D();

	// Draw the active app
	m_ActiveApp->onDraw();
}

void App::onGUI()
{
	// UI Drawing Set Up, must be called first
	m_graphics->RenderUI();

	// Draw the UI of the active app
	m_ActiveApp->onGUI();

	// Render everything to our screen, Must be called after all draw calls
	m_graphics->Render();
}