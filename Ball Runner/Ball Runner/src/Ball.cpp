#include "Ball.h"
#include <btBulletDynamicsCommon.h>

Ball::Ball()
{

}

Ball::~Ball()
{
	// Clean up after ourselves
}

void Ball::Initialise(std::shared_ptr<asset::ModelManager> &mManager, btDiscreteDynamicsWorld* const dynamicsWorld)
{
	// Set up our Model
	Setup(mManager, "assets/sphere.meshes", "assets/textures/wood.bmp", GL_TEXTURE_2D);
	// Set our model position
	m_pos = glm::vec3(0.0f, 2.0f, 0.0f);
	UpdateActor();

	// Create a collision shape
	btCollisionShape* colShape(new btSphereShape(1));

	// Set our collision variables

	btMotionState* MotionState(new btDefaultMotionState(btTransform(btQuaternion(m_rotate.x, m_rotate.y, m_rotate.z, 1), btVector3(m_pos.x, m_pos.y, m_pos.z))));
	btScalar mass = 1;
	btVector3 Inertia = btVector3(0, 0, 0);
	colShape->calculateLocalInertia(mass, Inertia);
	btRigidBody::btRigidBodyConstructionInfo* RigidBodyCI(new btRigidBody::btRigidBodyConstructionInfo(mass, MotionState, colShape, Inertia));
	m_RigidBody.reset(new btRigidBody(*RigidBodyCI));

	// Add our shape to the physics world
	dynamicsWorld->addRigidBody(m_RigidBody.get());
	// Prevent the object from deactivating in the Physics world
	m_RigidBody->setActivationState(DISABLE_DEACTIVATION);
}

void Ball::Update(float dt)
{
	// Update Call
	getTrans();
}

void Ball::Draw()
{
	// Pass our Model data to our Draw Function
	Draw3D();
}

void Ball::modifyPos(const glm::vec3 &pos)
{
	// Modoify the current ball position
	m_pos += pos;
}

void Ball::getTrans()
{
	// Get the Ball transform matrix from the Physics world
	btTransform trans;
	m_RigidBody->getMotionState()->getWorldTransform(trans);
	// Pass it to our model for drawing purposes
	trans.getOpenGLMatrix(glm::value_ptr(m_modelMatrix));
	// Get the ball Position
	btVector3 pos = trans.getOrigin();
	m_pos = glm::vec3(pos.getX(), pos.getY(), pos.getZ());
}

void Ball::resetBall()
{
	// Reset the ball matrix values (Scale not needed here)
	m_scale = glm::vec3(1.0f, 1.0f, 1.0f);
	m_rotate = glm::vec3(0.0f, 0.0f, 0.0f);
	m_pos = glm::vec3(0.0f, 2.0f, 0.0f);
	// Create our Matrix and use it
	UpdateActor();
	// Give the Matrix to the Physics world
	btTransform trans;
	float* data = glm::value_ptr(m_modelMatrix);
	trans.setFromOpenGLMatrix(data);
	m_RigidBody->setWorldTransform(trans);
	// Remove all forces affecting the ball
	m_RigidBody->setLinearVelocity(btVector3(0.0f, 0.0f, 0.0f));
	m_RigidBody->setAngularVelocity(btVector3(0.0f, 0.0f, 0.0f));
	m_RigidBody->clearForces();
}

void Ball::getAABB(btVector3 &min, btVector3 &max)
{
	// Get AABB collision box
	m_RigidBody->getAabb(min, max);
}