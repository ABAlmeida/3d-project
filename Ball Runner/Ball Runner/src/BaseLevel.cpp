#include "BaseLevel.h"
#include "Ball.h"
#include "Goal.h"
#include "input.h"
#include <btBulletDynamicsCommon.h>
#include "ModelManager.h"

BaseLevel::BaseLevel() : m_ball(new Ball()),
						m_goal(new Goal()),
						m_levelComplete(false)
{

}

BaseLevel::~BaseLevel()
{

}

void BaseLevel::onInitialise(std::shared_ptr<asset::ModelManager> &mManager, btDiscreteDynamicsWorld* dynamicsWorld)
{
	m_modelManager = mManager;

	m_ball->Initialise(m_modelManager, dynamicsWorld);
	m_goal->Initialise(m_modelManager, dynamicsWorld);
}

int BaseLevel::onInput(const int input)
{
	// Reset the ball on Space
	if (input == input::kSpace)
	{
		m_ball->resetBall();
	}
	// Exit the level on Escape
	if (input == input::kEsc || m_levelComplete)
	{
		m_ball->resetBall();
		m_levelComplete = false;
		return 1;
	}

	return -1;
}

void BaseLevel::onUpdate(const float dt)
{	
	btVector3 goalMin, goalMax, ballMin, ballMax;
	m_goal->getAABB(goalMin, goalMax);
	m_ball->getAABB(ballMin, ballMax);
	// Update Calls
	if (checkAABB(goalMin, goalMax, ballMin, ballMax))
	{
		m_levelComplete = true;
	}

	m_ball->Update(dt);
	m_goal->Update(dt);
}

void BaseLevel::onDraw()
{
	// Draw Calls
	m_ball->Draw();
	m_goal->Draw();
}

void BaseLevel::onGUI()
{

}

bool BaseLevel::checkAABB(const btVector3 &Amin, const btVector3 &Amax, const btVector3 &Bmin, const btVector3 &Bmax)
{
	btVector3 distance = Amax - Amin;
	distance *= 0.5f;
	distance += Amin;

	if (Bmin.getX() < distance.getX() &&
		Bmin.getY() < distance.getY() &&
		Bmin.getZ() < distance.getZ() &&
		distance.getX() < Bmax.getX() &&
		distance.getY() < Bmax.getY() &&
		distance.getZ() < Bmax.getZ())
	{
		return true;
	}

	return false;
}