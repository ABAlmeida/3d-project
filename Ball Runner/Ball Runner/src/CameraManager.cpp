#include "CameraManager.h"

namespace asset
{

CameraManager::CameraManager()
{

}

CameraManager::CameraManager(int w, int h) : m_camera(glm::vec3(0.0f, 20.0f, 20.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)),
											m_VO(new glm::mat4(1.0f)),
											m_view(new glm::mat4(1.0f)),
											m_projection(new glm::mat4(1.0f)),
											m_mvp(new glm::mat4(1.0f))
{
	// Set up our Perspective Projection Variables
	float aspectRatio = (float)w / (float)h;
	float zNear = 0.1f;
	float zFar = 100.0f;
	float FOV = 0.8f;

	// Set up our MVP variables
	glm::mat4 oView = glm::lookAt(glm::vec3(0.0f, -1.0f, 0.00001f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	float halfWidth = (float)w * 0.5f;
	float halfHeight = (float)h * 0.5f;
	glm::mat4 oProj = glm::ortho(-halfWidth, halfWidth, -halfHeight, halfHeight, 0.0f, 100.0f);
	*m_VO = oProj * oView;
	*m_projection = glm::perspective(FOV, aspectRatio, zNear, zFar);
}

CameraManager::~CameraManager()
{

}

glm::mat4& CameraManager::makeMVP(const glm::mat4 &model)
{
	// Create our View Matrix using GLM
	*m_view = glm::lookAt(m_camera.eye, m_camera.center, m_camera.up);
	// Create our MVP Matrix
	*m_mvp = *m_projection * *m_view * model;

	// Return our MVP matrix
	return *m_mvp;
}

void CameraManager::setCamera(const glm::vec3 &eye, const glm::vec3 &center)
{
	// Set the Camera
	m_camera.eye = eye;
	m_camera.center = center;
}

void CameraManager::modifyCamera(const glm::vec3 &modify)
{
	// Modify the camera
	m_camera.eye += modify;
	m_camera.center += modify;
}

void CameraManager::setEye(const glm::vec3 &eye)
{
	// Set the camera eye
	m_camera.eye = eye;
}

void CameraManager::modifyEye(const glm::vec3 &modify)
{
	// Modify the camera eye
	m_camera.eye += modify;
}

void CameraManager::setCenter(const glm::vec3 &center)
{
	// Set the camera center
	m_camera.center = center;
}

void CameraManager::modifyCenter(const glm::vec3 &modify)
{
	// Modify the camera center
	m_camera.center += modify;
}

}