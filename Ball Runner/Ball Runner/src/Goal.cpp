#include "Goal.h"
#include "TransformManager.h"
#include <btBulletDynamicsCommon.h>

Goal::Goal()
{

}

Goal::~Goal()
{

}

void Goal::Initialise(std::shared_ptr<asset::ModelManager> &mManager, btDiscreteDynamicsWorld* const dynamicsWorld)
{
	// Set up our Model
	Setup(mManager, "assets/Goal.meshes", "assets/textures/Planks.jpg", GL_TEXTURE_2D);
	m_scale = glm::vec3(1.0f, 1.0f, 1.0f);
	m_pos = glm::vec3(0.0f, 2.0f, -10.0f);

	// Create our Collision Shape
	btCompoundShape* colShape(new btCompoundShape());
	btTransform trans;

	// Make our first shape and add it
	btCollisionShape* first(new btBoxShape(btVector3(btScalar(1.5), btScalar(0.25), btScalar(0.25))));
	trans.setIdentity();
	trans.setOrigin(btVector3(0.0f, 1.25f, 0.0f));
	colShape->addChildShape(trans, first);

	// Make our second shape and add it
	btCollisionShape* second(new btBoxShape(btVector3(btScalar(0.25), btScalar(1.5), btScalar(0.25))));
	trans.setIdentity();
	trans.setOrigin(btVector3(-1.75f, 0.0f, 0.0f));
	colShape->addChildShape(trans, second);

	// Make our third collision shape and add it
	btCollisionShape* third(new btBoxShape(btVector3(btScalar(0.25), btScalar(1.5), btScalar(0.25))));
	trans.setIdentity();
	trans.setOrigin(btVector3(1.75f, 0.0f, 0.0f));
	colShape->addChildShape(trans, third);

	// Set our values for our collision Shape
	btMotionState* MotionState(new btDefaultMotionState(btTransform(btQuaternion(m_rotate.x, m_rotate.y, m_rotate.z, 1), btVector3(m_pos.x, m_pos.y, m_pos.z))));
	btScalar mass = 0;
	btVector3 Inertia = btVector3(0, 0, 0);
	btRigidBody::btRigidBodyConstructionInfo* RigidBodyCI(new btRigidBody::btRigidBodyConstructionInfo(mass, MotionState, colShape, Inertia));
	m_postsRB.reset(new btRigidBody(*RigidBodyCI));

	// Pass our Collision shape to our Physics world
	dynamicsWorld->addRigidBody(m_postsRB.get());

	// Create our Collision Shape
	btCollisionShape* colShape2(new btBoxShape(btVector3(btScalar(1.4), btScalar(0.9), btScalar(0.2))));

	// Set our values for our collision Shape
	btDefaultMotionState* MotionState2(new btDefaultMotionState(btTransform(btQuaternion(m_rotate.x, m_rotate.y, m_rotate.z, 1), btVector3(m_pos.x, m_pos.y, m_pos.z))));
	btScalar mass2 = 0;
	btVector3 Inertia2 = btVector3(0, 0, 0);
	btRigidBody::btRigidBodyConstructionInfo* RigidBodyCI2(new btRigidBody::btRigidBodyConstructionInfo(mass2, MotionState2, colShape2, Inertia2));
	m_goalRB.reset(new btRigidBody(*RigidBodyCI2));
	m_goalRB->setCollisionFlags(m_goalRB->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);

	// Pass our Collision shape to our Physics world
	dynamicsWorld->addRigidBody(m_goalRB.get());
}

void Goal::Update(float dt)
{
	// Update Call
	//getTrans();
}

void Goal::Draw()
{
	// Pass our Model data to our Draw Function
	Draw3D();
}

void Goal::getTrans()
{
	// Get the Ball transform matrix from the Physics world
	btTransform trans;
	m_goalRB->getMotionState()->getWorldTransform(trans);
	// Pass it to our model for drawing purposes
	trans.getOpenGLMatrix(glm::value_ptr(m_modelMatrix));
	// Get the ball Position
	btVector3 pos = trans.getOrigin();
	m_pos = glm::vec3(pos.getX(), pos.getY(), pos.getZ());
}

void Goal::setTrans(const glm::mat4 &newTrans)
{
	m_modelMatrix = newTrans * makeTranslationMatrix(m_pos);
	// Set our matrix for our Physics Engine
	btTransform trans;
	float* data = glm::value_ptr(m_modelMatrix);
	trans.setFromOpenGLMatrix(data);

	m_goalRB->setWorldTransform(trans);
	m_postsRB->setWorldTransform(trans);
}

void Goal::getAABB(btVector3 &min, btVector3 &max)
{
	m_goalRB->getAabb(min, max);
}