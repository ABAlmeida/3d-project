#include "Graphics.h"

Graphics::Graphics(const int width, const int height)
{
	// Initialize SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		// Something went very wrong in initialisation, all we can do is exit
		std::cout << "Whoops! SDL cannot initialise." << std::endl;
	}

	// Set SDL GL Values
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	// Create our window
	m_window = SDL_CreateWindow("Ball Runner", 150, 40, width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	if (m_window == NULL)
	{
		std::cout << "Whoops! SDL cannot create window." << std::endl;
	}

	// Create and Use our Context
	m_context = SDL_GL_CreateContext(m_window);
	if (m_context == NULL)
	{
		std::cout << "Whoops! SDL cannot create context." << std::endl;
	}
	SDL_GL_MakeCurrent(m_window, m_context);

	// Initialise Glew
	GLenum error = glewInit();
	if (error != GLEW_OK)
	{
		std::cout << "Whoops! Error initiating GLEW" << std::endl;
	}

	// Set our error checking variable
	error = GL_NO_ERROR;

	//Initialize clear color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	error = glGetError();
	if (error != GL_NO_ERROR)
	{
		std::cout << "Cannot Clear Colour." << std::endl;
	}

	//Clear the colour buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	// Accept fragment if it closer to the camera than the former one
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW); // counter clock wise, robs exporter is CCW
	glCullFace(GL_BACK);

	//Set the Viewport for OpenGL
	glViewport(0, 0, width, height);
};

Graphics::~Graphics()
{

};

void Graphics::Render()
{
	//Clear the colour of the screen to Black
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glFinish();
	//Swap the screen buffers
	SDL_GL_SwapWindow(m_window);

	// Clear the Colour and Depth Buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Graphics::Render3D()
{
	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW); // counter clock wise, robs exporter is CCW
	glCullFace(GL_BACK);
}

void Graphics::RenderUI()
{
	// Disable Depth Test
	glDisable(GL_DEPTH_TEST);
	// Disable Culling
	glDisable(GL_CULL_FACE);
}