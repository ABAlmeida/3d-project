#include "Input.h"

namespace input
{

inputReader::inputReader()
{

}

inputReader::~inputReader()
{

}

int inputReader::readInput(SDL_Event &incomingEvent)
{
	//Retrieve mouse position
	int x, y;
	SDL_GetMouseState(&x, &y);

	while (SDL_PollEvent(&incomingEvent))
	{
		// If we get in here, we have an event and need to figure out what to do with it
		// For now, we will just use a switch based on the event's type
		switch (incomingEvent.type)
		{
		case SDL_QUIT:
			// The event type is SDL_QUIT
			// This means we have been asked to quit - probably the user clicked on the 'x' at the top right corner of the window
			// To quit we need to set our 'go' bool to false so that we can escape out of the game loop
			return kEsc;
			break;

			//On Mouse Button Down
		case SDL_MOUSEBUTTONDOWN:
			switch (incomingEvent.button.button)
			{
			case SDL_BUTTON_LEFT:
				return kLMouse;
				break;
			case SDL_BUTTON_RIGHT:
				return kRMouse;
				break;
			case SDL_BUTTON_MIDDLE:
				return kMMouse;
				break;
			}

			//On Mouse Button Up
		case SDL_MOUSEBUTTONUP:
			switch (incomingEvent.button.button)
			{
			case SDL_BUTTON_LEFT:
				return -1;
				break;
			case SDL_BUTTON_RIGHT:
				return -1;
				break;
			case SDL_BUTTON_MIDDLE:
				return -1;
				break;
			}

			//On Key Press
		case SDL_KEYDOWN:
			switch (incomingEvent.key.keysym.sym)
			{
			case SDLK_UP:
				return kUArrow;
				break;
			case SDLK_LEFT:
				return kLArrow;
				break;
			case SDLK_RIGHT:
				return kRArrow;
				break;
			case SDLK_DOWN:
				return kDArrow;
				break;
			case SDLK_SPACE:
				return kSpace;
				break;
			case SDLK_RETURN:
				return kEnter;
				break;
			case SDLK_p:
				return kP;
				break;
			case SDLK_ESCAPE:
				return kEsc;
				break;
			}

			//On Key Release
		case SDL_KEYUP:
			switch (incomingEvent.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				break;
			}
		}
	}
	//Nothing pressed
	return -1;
}

}