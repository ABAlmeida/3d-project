#include "Level1.h"
#include "Ball.h"
#include "Goal.h"
#include "Input.h"
#include "Physics.h"
#include "Map.h"
#include "ModelManager.h"

Level1::Level1() : m_map(new Map())
{

}

Level1::~Level1()
{

}

void Level1::onInitialise(std::shared_ptr<asset::ModelManager> &mManager, btDiscreteDynamicsWorld* const dynamicsWorld)
{
	m_modelManager = mManager;

	m_ball->Initialise(m_modelManager, dynamicsWorld);
	lastBallpos = m_ball->getPos();
	m_map->Initialise(m_modelManager, dynamicsWorld);
	m_map->setMouse();
	m_goal->Initialise(m_modelManager, dynamicsWorld);
}

void Level1::onUpdate(float dt)
{
	btVector3 goalMin, goalMax, ballMin, ballMax;
	m_goal->getAABB(goalMin, goalMax);
	m_ball->getAABB(ballMin, ballMax);
	// Update Calls
	if (checkAABB(goalMin, goalMax, ballMin, ballMax))
	{
		m_levelComplete = true;
		m_map->resetMap();
	}
	glm::vec3 ballPos = m_ball->getPos();
	if (ballPos != lastBallpos)
	{
		glm::vec3 revBallDirection = glm::normalize(lastBallpos - ballPos);
		revBallDirection *= 10.0f;
		m_modelManager->setCamera(m_ball->getPos() + glm::vec3(0.0f, 10.0f, 10.0f), m_ball->getPos());
	}

	m_ball->Update(dt);
	m_map->modifyMap(m_ball->getPos());
	m_map->Update(dt);
	m_goal->setTrans(m_map->getTrans());
	m_goal->Update(dt);
}

void Level1::onDraw()
{
	// Draw Calls
	m_ball->Draw();
	m_map->Draw();
	m_goal->Draw();
}

void Level1::onGUI()
{
	// GUI Calls
}