#include "LightingProgram.h"
#include "model.h"

namespace shader
{

LightingProgram::LightingProgram()
{

}

LightingProgram::~LightingProgram()
{

}

void LightingProgram::initialiseProgram()
{
	makeShaderProgram();
	addShader("shaders/3Dshader.vs", GL_VERTEX_SHADER);
	addShader("shaders/3Dshader.fs", GL_FRAGMENT_SHADER);
	addShaderProgram();
	getUniformSlots();
}

void LightingProgram::getUniformSlots()
{
	//Retrieve our transform variable
	getUniform(m_mSlot, "v_Model");
	getUniform(m_mvpSlot, "v_MVP");
	getUniform(m_textureSlot, "Sampler");
	getUniform(m_aColourSlot, "f_aColour");
	getUniform(m_aIntensitySlot, "f_aIntensity");
	getUniform(m_dDirectionSlot, "f_dDirection");
	getUniform(m_dIntensitySlot, "f_dIntensity");
	getUniform(m_eyeSlot, "f_eye");
	getUniform(m_sPowerSlot, "f_sPower");
	getUniform(m_sIntensitySlot, "f_sIntensity");

	// Use our Texture Slot from our Shader Manager
	glUniform1i(m_textureSlot, 0);
}

void LightingProgram::useUniformValues(const glm::mat4 &mMatrix, const glm::mat4 &mvpMatrix, const asset::model &m, const glm::vec3 &eye)
{
	glUniformMatrix4fv(m_mvpSlot, 1, GL_FALSE, glm::value_ptr(mvpMatrix));
	glUniformMatrix4fv(m_mSlot, 1, GL_FALSE, glm::value_ptr(mMatrix));
	glUniform3f(m_aColourSlot, m.aColour.x, m.aColour.y, m.aColour.z);
	glUniform1f(m_aIntensitySlot, m.aIntensity);
	glUniform3f(m_dDirectionSlot, m.dDirection.x, m.dDirection.y, m.dDirection.z);
	glUniform1f(m_dIntensitySlot, m.dIntensity);
	glUniform3f(m_eyeSlot, eye.x, eye.y, eye.z); 
	glUniform1f(m_sPowerSlot, m.sPower); 
	glUniform1f(m_sIntensitySlot, m.sIntensity);
}

}