#include "Map.h"
#include "TransformManager.h"
#include <btBulletDynamicsCommon.h>

Map::Map() : m_localMatrix(glm::mat4(1.0f))
{
	// Set our starting mouse position
	SDL_GetMouseState(&mouse_x, &mouse_y);
}

Map::~Map()
{
	// Clean up our program
}

void Map::Initialise(std::shared_ptr<asset::ModelManager> &mManager, btDiscreteDynamicsWorld* const dynamicsWorld)
{
	// Set up our Model
	Setup(mManager, "assets/Level1.meshes", "assets/textures/checkers.bmp", GL_TEXTURE_2D);
	m_pos.z = -10.0f;

	// Create our Collision Shape
	btCollisionShape* colShape(new btBoxShape(btVector3(btScalar(4.5), btScalar(0.5), btScalar(13.))));

	// Set our values for our collision Shape
	btDefaultMotionState* MotionState(new btDefaultMotionState(btTransform(btQuaternion(m_rotate.x, m_rotate.y, m_rotate.z, 1), btVector3(m_pos.x, m_pos.y, m_pos.z))));
	btScalar mass = 0;
	btVector3 Inertia = btVector3(0, 0, 0);
	btRigidBody::btRigidBodyConstructionInfo* RigidBodyCI(new btRigidBody::btRigidBodyConstructionInfo(mass, MotionState, colShape, Inertia));
	m_RigidBody.reset(new btRigidBody(*RigidBodyCI));

	// Pass our Collision shape to our Physics world
	dynamicsWorld->addRigidBody(m_RigidBody.get());
}

void Map::Update(float dt)
{
	// Update Call
	setTrans();
}

void Map::Draw()
{
	// Pass our Model data to our Draw Function
	Draw3D();
}

void Map::modifyMap(glm::vec3 ball_pos)
{
	// Get the mouse Position
	int x, z;
	SDL_GetMouseState(&x, &z);
	// Set the ball Y to 0, we only want to translate the X and Z
	ball_pos.y = 0.0f;
	// Make sure we move our model to it's spawn point and correctly map the ball to this
	ball_pos -= m_pos;

	// Calculate how much to rotate the ball
	m_rotate += glm::vec3((float)(z - mouse_y), 0.0f, (float)(mouse_x - x));
	// Set the new mouse position
	mouse_x = x;
	mouse_y = z;

	// Don't allow rotations over 10
	if (m_rotate.x > 10.0f)
		m_rotate.x = 10.0f;
	if (m_rotate.x < -10.0f)
		m_rotate.x = -10.0f;
	if (m_rotate.z > 10.0f)
		m_rotate.z = 10.0f;
	if (m_rotate.z < -10.0f)
		m_rotate.z = -10.0f;

	// Move the model to our new pivot point
	glm::mat4 translate = makeTranslationMatrix(-ball_pos);
	// Rotate around it
	glm::mat4 rotate = makeRotationMatrix(m_rotate);
	// Multiply
	m_localMatrix = rotate * translate;
	// Move it back
	translate = makeTranslationMatrix(ball_pos);
	m_localMatrix = translate * m_localMatrix;
	// Set matrix
	m_modelMatrix = makeTranslationMatrix(m_pos) * m_localMatrix;
}

void Map::resetMap()
{
	// Get the mouse Position
	int x, z;
	SDL_GetMouseState(&x, &z);

	// Set the new mouse position
	mouse_x = x;
	mouse_y = z;

	// Reset rotations
	m_rotate = glm::vec3(0.0f, 0.0f, 0.0f);
}

void Map::setTrans()
{
	// Set our matrix for our Physics Engine
	btTransform trans;
	float* data = glm::value_ptr(m_modelMatrix);
	trans.setFromOpenGLMatrix(data);

	m_RigidBody->setWorldTransform(trans);
}

glm::mat4 Map::getTrans()
{
	// Get the model matrix
	return m_modelMatrix;
}