#include "Menu.h"
#include "Input.h"
#include "Sprite.h"
#include "ModelManager.h"

Menu::Menu() : m_selected(1)
{

}

Menu::~Menu()
{

}

void Menu::onInitialise(std::shared_ptr<asset::ModelManager> &mManager)
{
	// By default, set selected to play

	// Initialise all of our sprites
	m_backdrop.reset(new Sprite(mManager, "assets/textures/menu.png"));
	m_play.reset(new Sprite(mManager, "assets/textures/play.png"));
	m_play->pos = glm::vec2(0.5f, 0.5f);
	m_playdown.reset(new Sprite(mManager, "assets/textures/playdown.png"));
	m_playdown->pos = glm::vec2(0.5f, 0.5f);
	m_quit.reset(new Sprite(mManager, "assets/textures/quit.png"));
	m_quit->pos = glm::vec2(0.5f, 0.3f);
	m_quitdown.reset(new Sprite(mManager, "assets/textures/quitdown.png"));
	m_quitdown->pos = glm::vec2(0.5f, 0.3f);
}

int Menu::onInput(const int Input)
{
	// If up is pressed
	if (Input == input::kUArrow)
	{
		m_selected--;
		if (m_selected < 1)
		{
			m_selected = 1;
		}
	}

	// If down is pressed
	if (Input == input::kDArrow)
	{
		m_selected++;
		if (m_selected > 2)
		{
			m_selected = 2;
		}
	}

	// If enter is pressed
	if (Input == input::kEnter)
	{
		if (m_selected == 1)
		{
			return 2;
		}
		if (m_selected == 2)
		{
			return 0;
		}
	}
	// Return a NULL Value
	return -1;
}

void Menu::onUpdate(const float dt)
{

}

void Menu::onDraw()
{
	
}

void Menu::onGUI()
{
	// UI Draw Calls
	m_backdrop->Draw();
	m_play->Draw();
	m_quit->Draw();

	// Whichever button is selected, draw down copy of it
	switch (m_selected)
	{
	case 1:
		m_playdown->Draw();
		break;
	case 2:
		m_quitdown->Draw();
		break;
	}
}