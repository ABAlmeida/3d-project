#include "MeshManager.h"

namespace asset
{

MeshManager::MeshManager()
{

}

MeshManager::~MeshManager()
{

}

int MeshManager::loadModel(const std::string &file)
{
	// Create a temporary Components holder
	mData temp;

	// Load stuff into our Temp holder
	if (loadFromMesh(file, temp))
	{
		// If returned true, store our mesh
		m_models.push_back(temp);
		// Return the Model index
		return (m_models.size() - 1);
	}
	// Return -1 for model index does not exist, model failed to load
	return -1;
}

bool MeshManager::loadModelComponents(int i)
{
	//Generate the Vertex Buffer (1, at the VBO address)
	glGenBuffers(1, &m_VBO);
	//Bind the Array buffer to the VBO, because it's holding an array of vertices
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	//Bind the Data to the Vertex Buffer (Arrays, Size, What to bind, Static because the buffer contents do not change)
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex)*m_models[i].vertices.size(), &m_models[i].vertices[0], GL_STATIC_DRAW);
	//Unbind the buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Generate the Index Buffer (1, at the VBO address)
	glGenBuffers(1, &m_IBO);
	//Bind the Array buffer to the VBO, because it's holding an array of vertices
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	//Bind the Data to the Vertex Buffer (Arrays, Size, What to bind, Static because the buffer contents do not change)
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_models[i].indices.size(), &m_models[i].indices[0], GL_STATIC_DRAW);
	//Unbind the buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	return true;
}

bool MeshManager::loadFromMesh(const std::string &mesh, mData &components)
{
	//initialising variables
	std::string content;

	// load the string into the buffer
	loadString(mesh, content);

	//Check for null
	if (content.empty())
	{
		std::cout << ".meshes file empty\n";
		return false;
	}

	//intialise variables
	int numMesh;						//Number of Meshes (If greater than 1, this code needs to be revisited)
	char* nameMesh = new char[1024];	//Name of Mesh
	int numVertex;						//Number of Vertices
	int numIndex;						//Number of Normals

	float irrelevant;

	//reading in the file
	std::istringstream iss(content);
	//read in: number of meshes, name of mesh, number of vertices, number of indices
	iss >> numMesh;

	for (int l = 0; l < numMesh; l++)
	{
		iss >> nameMesh >> numVertex >> numIndex;
		//We don't actually need the name of the mesh here

		int tempVsize = components.vertices.size();
		int tempIsize = components.indices.size();

		// resize vectors
		components.vertices.resize(tempVsize + numVertex);
		components.indices.resize(tempIsize + numIndex);


		// read in vertex
		for (unsigned int i = tempVsize; i < components.vertices.size(); i++)
		{
			iss >> components.vertices[i].pos.x >> components.vertices[i].pos.y >> components.vertices[i].pos.z				// Vertices
				>> components.vertices[i].normal.x >> components.vertices[i].normal.y >> components.vertices[i].normal.z		// Normals
				>> irrelevant >> irrelevant >> irrelevant																		// Tangents
				>> irrelevant >> irrelevant >> irrelevant																		// Bi-normals
				>> components.vertices[i].uv.x >> components.vertices[i].uv.y;												// UVs
		}

		// read in vertex index data
		for (unsigned int i = tempIsize; i < components.indices.size(); i++)
		{
			iss >> components.indices[i];
			components.indices[i] += tempVsize;
		}
	}

	delete[] nameMesh;

	return true;
}

void MeshManager::loadString(const std::string &file, std::string &returnData) const
{
	//Create a string to read data into
	std::string data;
	//Choose the file you want to read in
	std::ifstream readFile(file.c_str());
	//If the file opened correctly, retrieve the data from it
	if (readFile.is_open())
	{
		//Whilst there is data copy it over
		while (getline(readFile, data))
		{
			//Store the data in our return string
			returnData.append(data);
			returnData.append("\n");
		}
		readFile.close();
	}
	else
	{
		std::cout << "Unable to open file";
	}
}

}