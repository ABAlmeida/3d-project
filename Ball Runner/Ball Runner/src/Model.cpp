#include "Model.h"

namespace asset
{

model::model()
{
	aColour = glm::vec3(1.0f, 1.0f, 1.0f);
	aIntensity = 0.5f;
	dDirection = glm::normalize(glm::vec3(1.0f, -1.0f, -0.3f));
	dIntensity = 0.2f;
	sPower = 32.0f;
	sIntensity = 0.2f;
}

model::~model()
{

}

}