#include "ModelManager.h"
#include "Model.h"
#include "MeshManager.h"
#include "TextureManager.h"
#include "ShaderManager.h"
#include "CameraManager.h"

namespace asset
{

ModelManager::ModelManager() : m_meshManager(new MeshManager()),
								m_textureManager(new TextureManager()),
								m_shaderManager(new shader::ShaderManager()),
								m_cameraManager(new CameraManager(1080, 720))
{
	// Sprite Data
	int modelIndex = m_meshManager->loadModel("assets/spritecube.meshes");
	// Load our mesh data into OpenGL
	m_meshManager->loadModelComponents(modelIndex);
	// Get our VBO for our Model
	m_spriteVBO = m_meshManager->getVBO();
	// Get our IBO for our Model
	m_spriteIBO = m_meshManager->getIBO();
	// Get our Index Count for our Model
	m_spriteCount = m_meshManager->getIndexCount(modelIndex);
}

ModelManager::~ModelManager()
{

}

void ModelManager::loadMesh(model &m, const char* const meshpath)
{
	// Get our mesh index for our Loaded Data
	int modelIndex = m_meshManager->loadModel(meshpath);
	// Load our mesh data into OpenGL
	m_meshManager->loadModelComponents(modelIndex);
	// Get our VBO for our Model
	m.VBO = m_meshManager->getVBO();
	// Get our IBO for our Model
	m.IBO = m_meshManager->getIBO();
	// Get our Index Count for our Model
	m.indexCount = m_meshManager->getIndexCount(modelIndex);
}

void ModelManager::loadTexture(model &m, const char* const texturepath, const GLenum textureTarget)
{
	// Get our mesh index for our Loaded Data
	int textureIndex = m_textureManager->loadTexture(texturepath);
	// Load our mesh data into OpenGL
	m_textureManager->loadTextureComponents(textureIndex, textureTarget);
	// Get our VBO for our Model
	m.TBO = m_textureManager->getTBO();
	// Get our IBO for our Model
	m.TT = m_textureManager->getTT();
}

void ModelManager::loadSprite(UI &ui, const char* const filepath)
{
	// Get our mesh index for our Loaded Data
	int textureIndex = m_textureManager->loadTexture(filepath);
	// Load our mesh data into OpenGL
	m_textureManager->loadTextureComponents(textureIndex, GL_TEXTURE_2D);
	ui.TBO = m_textureManager->getTBO();

	tData ttemp = m_textureManager->getTData();
	ui.VBO = m_spriteVBO;
	ui.IBO = m_spriteIBO;
	ui.indexCount = m_spriteCount;
	ui.height = ttemp.height;
	ui.width = ttemp.width;
}

void ModelManager::drawModel(const model &m, const glm::mat4 &mMatrix)
{
	glm::mat4 mvp = m_cameraManager->makeMVP(mMatrix);
	m_shaderManager->useLightingProgram(mMatrix, mvp, m, m_cameraManager->getEye());

	//Enable the vertex arrays within GL
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	//Bind the Buffer to the Vertex Buffer Object
	glBindBuffer(GL_ARRAY_BUFFER, m.VBO);
	//How to interpret the data in the buffer
	//					 (Shader Index, Number of Components XYZ, Data Type Components,
	//					  Need Normalized?, Stride Bytes between instances, Offset of Component)
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), (const GLvoid*)12);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (const GLvoid*)20);
	//Bind our Element Array buffer to our Index Buffer Object
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m.IBO);

	// Set our Active Texture
	glActiveTexture(GL_TEXTURE0);
	// Bind our TBO
	glBindTexture(m.TT, m.TBO);

	//Draw call for Elements this time
	//(Render Type, Number of Indices, Index Type, Offset Bytes)
	glDrawElements(GL_TRIANGLES, m.indexCount, GL_UNSIGNED_INT, 0);

	// Unbind our Buffers
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	//Disable the Vertex Attribute when it's not being used
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}

void ModelManager::drawUI(const UI &ui, const glm::mat4 &mMatrix)
{
	glm::mat4 mvp = mMatrix * m_cameraManager->getVO();
	//glm::mat4 mvp = m_cameraManager->makeMVP(mMatrix);
	m_shaderManager->useUIProgram(mvp);

	//Enable the vertex arrays within GL
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	//Bind the Buffer to the Vertex Buffer Object
	glBindBuffer(GL_ARRAY_BUFFER, ui.VBO);
	//How to interpret the data in the buffer
	//					 (Shader Index, Number of Components XYZ, Data Type Components,
	//					  Need Normalized?, Stride Bytes between instances, Offset of Component)
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), (const GLvoid*)12);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (const GLvoid*)20);
	//Bind our Element Array buffer to our Index Buffer Object
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ui.IBO);

	// Set our Active Texture
	glActiveTexture(GL_TEXTURE0);
	// Bind our TBO
	glBindTexture(GL_TEXTURE_2D, ui.TBO);

	//Draw call for Elements this time
	//(Render Type, Number of Indices, Index Type, Offset Bytes)
	glDrawElements(GL_TRIANGLES, ui.indexCount, GL_UNSIGNED_INT, 0);

	// Unbind our Buffers
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	//Disable the Vertex Attribute when it's not being used
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}

void ModelManager::setCamera(const glm::vec3 &eye, const glm::vec3 &center)
{
	m_cameraManager->setCamera(eye, center);
}

void ModelManager::modifyCamera(const glm::vec3 &eye, const glm::vec3 &center)
{
	m_cameraManager->modifyEye(eye);
	m_cameraManager->modifyCenter(center);
}

}