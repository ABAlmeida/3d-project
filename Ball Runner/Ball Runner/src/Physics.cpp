#include "Physics.h"

bPhysics::bPhysics() : m_config(new btDefaultCollisionConfiguration()),
						m_dispatcher(new btCollisionDispatcher(m_config.get())),
						m_oPC(new btDbvtBroadphase()),
						m_solver(new btSequentialImpulseConstraintSolver()),
						m_dynamicsWorld(new btDiscreteDynamicsWorld(m_dispatcher.get(), m_oPC.get(), m_solver.get(), m_config.get()))
{
	// Set the Gravity for the world
	m_dynamicsWorld->setGravity(btVector3(0, -10, 0));
}

bPhysics::~bPhysics()
{

}