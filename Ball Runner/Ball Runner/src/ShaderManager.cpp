#include "ShaderManager.h"
#include "LightingProgram.h"
#include "UIProgram.h"

namespace shader
{

ShaderManager::ShaderManager() : m_lightingProgram(new LightingProgram()),
								m_uiProgram(new UIProgram())
{
	m_lightingProgram->initialiseProgram();
	m_uiProgram->initialiseProgram();
}

ShaderManager::~ShaderManager()
{

}

bool ShaderManager::useLightingProgram(const glm::mat4 &mMatrix, const glm::mat4 &mvpMatrix, const asset::model &m, const glm::vec3 &eye)
{
	m_lightingProgram->useShaderProgram();
	m_lightingProgram->useUniformValues(mMatrix, mvpMatrix, m, eye);

	return true;
}

bool ShaderManager::useUIProgram(const glm::mat4 &mvpMatrix)
{
	m_uiProgram->useShaderProgram();
	m_uiProgram->useUniformValues(mvpMatrix);

	return true;
}

}