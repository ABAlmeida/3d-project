#include "ShaderProgram.h"

namespace shader
{

ShaderProgram::ShaderProgram()
{
	m_Success = 0;
}

ShaderProgram::~ShaderProgram()
{

}

bool ShaderProgram::makeShaderProgram()
{
	//Create the Shader program
	m_shaderProgram = glCreateProgram();

	//If the shader program fails to create display the error
	if (m_shaderProgram == 0)
	{
		std::cout << "Error creating m_ShaderProgram" << std::endl;
		return false;
	}
	else
	{
		return true;
	}
}

bool ShaderProgram::addShaderProgram()
{
	//Links the ShaderProgram to the Program
	glLinkProgram(m_shaderProgram);
	//Check to see if the program linked and loaded properly
	glGetProgramiv(m_shaderProgram, GL_LINK_STATUS, &m_Success);
	GLchar ErrorLog[1024] = { 0 };
	if (m_Success == 0)
	{
		glGetProgramInfoLog(m_shaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << "Error linking shader program: " << ErrorLog << std::endl;
		return false;
	}

	//Checks whether the program can run in the current state
	glValidateProgram(m_shaderProgram);
	//Displays the errors if it fails
	glGetProgramiv(m_shaderProgram, GL_VALIDATE_STATUS, &m_Success);
	if (!m_Success)
	{
		glGetProgramInfoLog(m_shaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << "Invalid shader program: " << ErrorLog << std::endl;
		return false;
	}

	//Uses the shader for all draw calls until disabled or replaced
	glUseProgram(m_shaderProgram);
	return true;
}

bool ShaderProgram::useShaderProgram()
{
	glUseProgram(m_shaderProgram);
	return true;
}

bool ShaderProgram::addShader(const char* const ShaderName, const GLenum ShaderType)
{
	//Creates the Shader Objects, the Vertex Shader and the Fragment Shader
	GLuint ShaderObject = glCreateShader(ShaderType);
	std::string ShaderText;

	//Checks in the Shader Objects are NULL, Error checking
	if (ShaderObject == 0)
	{
		std::cout << "Error creating ShaderObject" << std::endl;
		return false;
	}

	if (!loadShader(ShaderName, ShaderText))
	{
		std::cout << "loadShader failed to load Shader." << std::endl;
	}

	//A Char Ptr
	const GLchar* p[1];
	//Pointing to the Shader
	p[0] = ShaderText.c_str();
	//Create an array of 1 Int
	GLint Lengths[1];
	//Sets the value of the length to the size of the Shader
	Lengths[0] = strlen(ShaderText.c_str());
	//Specify the Shader Source
	//(Shader, Number of Slots, Shader Name, Length of Shader Name)
	glShaderSource(ShaderObject, 1, p, Lengths);
	//Compile the Shader
	glCompileShader(ShaderObject);
	//Check to make sure the Shader has compiled, displays any errors prevent compilation
	GLchar ErrorLog[1024] = { 0 };
	glGetShaderiv(ShaderObject, GL_COMPILE_STATUS, &m_Success);
	if (!m_Success)
	{
		glGetShaderInfoLog(ShaderObject, 1024, NULL, ErrorLog);
		std::cout << "Error compiling ShaderType " << ShaderType << ": " << ErrorLog << std::endl;
		return false;
	}

	//Similar to specifying the	Objects linking in a makefile
	//Attach the Shader Object to the Vertex Array Object
	glAttachShader(m_shaderProgram, ShaderObject);

	return true;
}

bool ShaderProgram::loadShader(const char* const charVar, std::string &stringVar)
{
	// Create an input stream for our shader
	std::ifstream f(charVar);

	// Initialise a false variable
	bool fileLoaded = false;

	// If we can open the file, start reading it in
	if (f.is_open())
	{
		std::string ShaderString;
		while (getline(f, ShaderString))
		{
			stringVar.append(ShaderString);
			stringVar.append("\n");
		}

		// Close the file
		f.close();

		// File has successfully loaded
		fileLoaded = true;
	}
	else
	{
		// Print our an error saying we couldn't load the shader
		std::cout << "Error loading Shader" << std::endl;
	}

	// Return whether the FileLoaded or not
	return fileLoaded;
}

void ShaderProgram::getUniform(GLint &slot, const char* const shaderName)
{
	slot = glGetUniformLocation(m_shaderProgram, shaderName);
	assert(slot != 0xFFFFFFFF);
}

}