#include "Sprite.h"
#include "Model.h"
#include "ModelManager.h"
#include "TransformManager.h"

Sprite::Sprite(std::shared_ptr<asset::ModelManager> &mManager, const char *filepath) : pos(glm::vec2(0.0f, 0.0f)),
																	m_scale(),
																	m_rotate(glm::vec3(0.0f, 0.0f, 0.0f)),
																	m_pos(glm::vec3(0.0f, 0.0f, 0.0f)),
																	m_modelMatrix(),
																	m_modelManager(mManager),
																	m_UI(new asset::UI())
{
	m_modelManager->loadSprite(*m_UI, filepath);
	m_scale = glm::vec3(m_UI->width, m_UI->height, 1.0f);
	m_modelMatrix = makeMatrix(m_scale, m_rotate, m_pos);
}

Sprite::~Sprite()
{

}

void Sprite::Draw()
{
	m_pos = glm::vec3(pos, 0.0f);
	m_modelMatrix = makeMatrix(m_scale, m_rotate, m_pos);
	m_modelManager->drawUI(*m_UI, m_modelMatrix);
}