#include "TextureManager.h"

namespace asset
{

TextureManager::TextureManager()
{

}

TextureManager::~TextureManager()
{

}

int TextureManager::loadTexture(const char *file)
{
	// Create a temporary instance of Texture Data
	tData temp;
	if (loadImage(file, &temp))
	{
		// If it passes, store our texture data and return our index
		m_textures.push_back(temp);
		return (m_textures.size() - 1);
	}
	// Return an impossible Index if it fails
	return -1;
}

bool TextureManager::loadTextureComponents(int i, const GLenum textureTarget)
{
	m_TT = textureTarget;

	// Generate a Texture Buffer at the address of our Texture Buffer Object
	glGenTextures(1, &m_TBO);
	// Bind the 2D Texture to our Texture Buffer Object
	glBindTexture(m_TT, m_TBO);
	// Bind the data to our Texture Buffer Object
	// (Texture Target, Level, Internal Format, Width, Height, Border, Format, Type, Pixels)  
	glTexImage2D(m_TT, 0, m_textures[i].BytesPerPixel, m_textures[i].width, m_textures[i].height, 0, m_textures[i].format, GL_UNSIGNED_BYTE, m_textures[i].pixels);
	// Texture Parameters
	glTexParameteri(m_TT, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(m_TT, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// Unbind our Buffer Object
	glBindTexture(m_TT, 0);

	// Return True
	return true;
}

bool TextureManager::loadImage(const char* FilePath, tData* const data)
{
	SDL_Surface *mySurface = IMG_Load(FilePath);

	// If we can load the BMP, get the Texture Data
	if (mySurface != NULL)
	{

		// Check that the image's width is a power of 2
		if ((mySurface->w & (mySurface->w - 1)) != 0)
		{
			std::cout << "warning: image.bmp's width is not a power of 2" << std::endl;
		}
		// Store our Data Width
		data->width = mySurface->w;

		// Also check if the height is a power of 2
		if ((mySurface->h & (mySurface->h - 1)) != 0)
		{
			std::cout << "warning: image.bmp's height is not a power of 2" << std::endl;
		}
		// Store our Data Height
		data->height = mySurface->h;

		// get the number of channels in the SDL surface
		data->BytesPerPixel = mySurface->format->BytesPerPixel;
		if (data->BytesPerPixel == 4)     // contains an alpha channel
		{
			if (mySurface->format->Rmask == 0x000000ff)
				data->format = GL_RGBA;
			else
				data->format = GL_BGRA;
		}
		else if (data->BytesPerPixel == 3)     // no alpha channel
		{
			if (mySurface->format->Rmask == 0x000000ff)
				data->format = GL_RGB;
			else
				data->format = GL_BGR;
		}
		else
		{
			std::cout << "warning: the image is not truecolor..  this will probably break\n" << std::endl;
			// this error should not go unhandled
		}
	}
	else
	{
		std::cout << "SDL could not load image.bmp: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}
	// Get our Pixel Data
	data->pixels = mySurface->pixels;
	return true;
}

}