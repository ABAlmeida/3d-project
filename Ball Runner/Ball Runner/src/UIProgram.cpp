#include "UIProgram.h"

namespace shader
{

UIProgram::UIProgram()
{

}

UIProgram::~UIProgram()
{

}

void UIProgram::initialiseProgram()
{
	makeShaderProgram();
	addShader("shaders/2Dshader.vs", GL_VERTEX_SHADER);
	addShader("shaders/2Dshader.fs", GL_FRAGMENT_SHADER);
	addShaderProgram();
	getUniformSlots();
}

void UIProgram::getUniformSlots()
{
	//Retrieve our transform variable
	m_mvpSlot = glGetUniformLocation(m_shaderProgram, "v_MVP");
	assert(m_mvpSlot != 0xFFFFFFFF);
	m_textureSlot = glGetUniformLocation(m_shaderProgram, "Sampler");
	assert(m_textureSlot != 0xFFFFFFFF);

	// Use our Texture Slot from our Shader Manager
	glUniform1i(m_textureSlot, 0);
}

void UIProgram::useUniformValues(const glm::mat4 &mvpMatrix)
{
	glUniformMatrix4fv(m_mvpSlot, 1, GL_FALSE, glm::value_ptr(mvpMatrix));
}

}