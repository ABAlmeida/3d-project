#pragma once
#include <memory>
#include "Application.h"

int main(int argc, char* argv[])
{	
	// Set the Screen Properties
	const int width = 1080;
	const int height = 720;
	
	// Create our Application
	std::unique_ptr<App> myApp(new App);

	// Initialise our Application
	myApp->onInitialise(width, height);
	// Update our Application
	while (myApp->onInput())
	{
		myApp->onUpdate();
		myApp->onDraw();
		myApp->onGUI();
	}

	return 0;
}